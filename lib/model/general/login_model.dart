class Login {
  String ret = "";
  late Data data;

  Login({required this.ret, required this.data});

  Login.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new Data.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  late String token;
  late int tokenExp;
  late User user;

  Data({required this.token, required this.tokenExp, required this.user});

  Data.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    tokenExp = json['tokenExp'];
    user = (json['user'] != null ? new User.fromJson(json['user']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['tokenExp'] = this.tokenExp;
    if (this.user != null) {
      data['user'] = this.user.toJson();
    }
    return data;
  }
}

class User {
  late int userId;
  late String email;
  late String name;
  late String phoneCode;
  late String mobile;
  late int role;
  late int companyId;
  Null apiKey;

  User(
      {required this.userId,
        required this.email,
        required this.name,
        required this.phoneCode,
        required this.mobile,
        required this.role,
        required this.companyId,
        this.apiKey});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    email = json['email'];
    name = json['name'];
    phoneCode = json['phoneCode'];
    mobile = json['mobile'];
    role = json['role'];
    companyId = json['companyId'];
    apiKey = json['apiKey'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['email'] = this.email;
    data['name'] = this.name;
    data['phoneCode'] = this.phoneCode;
    data['mobile'] = this.mobile;
    data['role'] = this.role;
    data['companyId'] = this.companyId;
    data['apiKey'] = this.apiKey;
    return data;
  }
}
