class Warehouse {
  late String ret;
  late Data data;

  Warehouse({required this.ret, required this.data});

  Warehouse.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new Data.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  late int totalCount;
  late List<Records> records;

  Data({required this.totalCount, required this.records});

  Data.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <Records>[];
      json['records'].forEach((v) {
        records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  late int id;
  late int companyId;
  late String name;
  Null type;
  late int status;
  late String cby;
  Null mby;
  late String cdate;
  Null mdate;

  Records(
      {required this.id,
        required this.companyId,
        required this.name,
        this.type,
        required this.status,
        required this.cby,
        this.mby,
        required this.cdate,
        this.mdate});

  Records.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    name = json['name'];
    type = json['type'];
    status = json['status'];
    cby = json['cby'];
    mby = json['mby'];
    cdate = json['cdate'];
    mdate = json['mdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['name'] = this.name;
    data['type'] = this.type;
    data['status'] = this.status;
    data['cby'] = this.cby;
    data['mby'] = this.mby;
    data['cdate'] = this.cdate;
    data['mdate'] = this.mdate;
    return data;
  }
}