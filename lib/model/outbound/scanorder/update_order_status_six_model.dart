class UpdateOrderStatusSix {
  late String ret;
  late UpdateOrderStatusSixData data;

  UpdateOrderStatusSix({required this.ret, required this.data});

  UpdateOrderStatusSix.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? UpdateOrderStatusSixData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UpdateOrderStatusSixData {
  late int updatedRow;

  UpdateOrderStatusSixData({required this.updatedRow});

  UpdateOrderStatusSixData.fromJson(Map<String, dynamic> json) {
    updatedRow = json['updatedRow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updatedRow'] = this.updatedRow;
    return data;
  }
}