class GetTrayInfo {
  late String ret;
  late GetTrayInfoData data;

  GetTrayInfo({required this.ret, required this.data});

  GetTrayInfo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetTrayInfoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetTrayInfoData {
  late int totalCount;
  late List<Records> records;

  GetTrayInfoData({required this.totalCount, required this.records});

  GetTrayInfoData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <Records>[];
      json['records'].forEach((v) {
        records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  late int id;
  late int companyId;
  late String trayCode;
  late int status;

  Records({required this.id, required this.companyId, required this.trayCode, required this.status});

  Records.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    trayCode = json['tray_code'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['tray_code'] = this.trayCode;
    data['status'] = this.status;
    return data;
  }
}