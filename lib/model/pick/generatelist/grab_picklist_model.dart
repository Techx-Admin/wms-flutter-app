class GrabPicklist {
  late String ret;
  late GrabPicklistData data;

  GrabPicklist({required this.ret, required this.data});

  GrabPicklist.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new GrabPicklistData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GrabPicklistData {
  late int orderPickId;

  GrabPicklistData({required this.orderPickId});

  GrabPicklistData.fromJson(Map<String, dynamic> json) {
    orderPickId = json['orderPickId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['orderPickId'] = this.orderPickId;
    return data;
  }
}