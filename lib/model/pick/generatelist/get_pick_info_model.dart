class GetPickInfo {
  late String ret;
  late GetPickInfoData data;

  GetPickInfo({required this.ret, required this.data});

  GetPickInfo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new GetPickInfoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetPickInfoData {
  late int userCompleted;
  late int availableOrderPick;

  GetPickInfoData({required this.userCompleted, required this.availableOrderPick});

  GetPickInfoData.fromJson(Map<String, dynamic> json) {
    userCompleted = json['userCompleted'];
    availableOrderPick = json['availableOrderPick'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userCompleted'] = this.userCompleted;
    data['availableOrderPick'] = this.availableOrderPick;
    return data;
  }
}