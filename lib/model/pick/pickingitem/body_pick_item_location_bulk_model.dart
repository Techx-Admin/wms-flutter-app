class Pick {
  late String locationId;
  late String pickedQty;
  late String status;

  Pick(this.locationId, this.pickedQty, this.status);

  Map toJson() => {
    'orderPickItemLocationId': locationId,
    'pickedQty': pickedQty,
    'status': status,
  };
}

class Bulk {
  late List<Pick> bulkData;

  Bulk(this.bulkData);

  Map toJson() => {
    'bulkData': bulkData
  };
}