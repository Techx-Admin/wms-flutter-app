class GetInbRequestDetail {
  late String ret;
  late GetInbRequestDetailData data;

  GetInbRequestDetail({required this.ret, required this.data});

  GetInbRequestDetail.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbRequestDetailData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbRequestDetailData {
  late List<GetInbRequestDetailItem> inboundRequestItem;

  GetInbRequestDetailData({required this.inboundRequestItem});

  GetInbRequestDetailData.fromJson(Map<String, dynamic> json) {
    if (json['inboundRequestItem'] != null) {
      inboundRequestItem = <GetInbRequestDetailItem>[];
      json['inboundRequestItem'].forEach((v) {
        inboundRequestItem.add(new GetInbRequestDetailItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.inboundRequestItem != null) {
      data['inboundRequestItem'] =
          this.inboundRequestItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbRequestDetailItem {
  late int id;
  late int requestId;
  late int skuId;
  late int qty;
  late String remarks;
  late int status;
  late GetInbRequestDetailItemSku sku;

  GetInbRequestDetailItem(
      {required this.id,
        required this.requestId,
        required this.skuId,
        required this.qty,
        required this.remarks,
        required this.status,
        required this.sku});

  GetInbRequestDetailItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    requestId = json['request_id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    remarks = json['remarks'];
    status = json['status'];
    sku = (json['sku'] != null ? GetInbRequestDetailItemSku.fromJson(json['sku']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['request_id'] = this.requestId;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['remarks'] = this.remarks;
    data['status'] = this.status;
    if (this.sku != null) {
      data['sku'] = this.sku.toJson();
    }
    return data;
  }
}

class GetInbRequestDetailItemSku {
  late int id;
  late String skuCode;
  late String skuName;
  late String barcode1;
  late int companyId;
  late int clientId;
  late int status;
  late String remarks;
  GetInbRequestDetailItemSkuBatch? skuBatch;

  GetInbRequestDetailItemSku(
      {required this.id,
        required this.skuCode,
        required this.skuName,
        required this.barcode1,
        required this.companyId,
        required this.clientId,
        required this.status,
        required this.remarks,
        required this.skuBatch});

  GetInbRequestDetailItemSku.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuCode = json['sku_code'];
    skuName = json['sku_name'];
    barcode1 = json['barcode_1'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    status = json['status'];
    remarks = json['remarks'];

    try {
      if(json['sku_batch'] == null) {

      }
      else {
        skuBatch = (json['sku_batch'] != null
            ? GetInbRequestDetailItemSkuBatch.fromJson(json['sku_batch'])
            : null)!;
      }
    } on Exception catch (e) {
      print(e.toString());
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_code'] = this.skuCode;
    data['sku_name'] = this.skuName;
    data['barcode_1'] = this.barcode1;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['status'] = this.status;
    data['remarks'] = this.remarks;
    if (this.skuBatch != null) {
      data['sku_batch'] = this.skuBatch?.toJson();
    }
    return data;
  }
}

class GetInbRequestDetailItemSkuBatch {
  late int id;
  late int skuId;
  late int qty;
  late int status;

  GetInbRequestDetailItemSkuBatch({required this.id, required this.skuId, required this.qty, required this.status});

  GetInbRequestDetailItemSkuBatch.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    return data;
  }
}