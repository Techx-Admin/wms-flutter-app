class GetInbBatchLocation {
  late String ret;
  late GetInbBatchLocationData data;

  GetInbBatchLocation({required this.ret, required this.data});

  GetInbBatchLocation.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbBatchLocationData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbBatchLocationData {
  late int totalCount;
  late List<GetInbBatchLocationRecords> records;

  GetInbBatchLocationData({required this.totalCount, required this.records});

  GetInbBatchLocationData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <GetInbBatchLocationRecords>[];
      json['records'].forEach((v) {
        records.add(new GetInbBatchLocationRecords.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbBatchLocationRecords {
  late int id;
  late int batchId;
  late int skuLocationId;
  late int containerRunningCodeId;
  late int locationId;
  late int batchQty;
  late int status;
  late String locationLabel;
  late String containerRunningCode;

  GetInbBatchLocationRecords(
      {required this.id,
        required this.batchId,
        required this.skuLocationId,
        required this.containerRunningCodeId,
        required this.locationId,
        required this.batchQty,
        required this.status,
        required this.locationLabel,
        required this.containerRunningCode});

  GetInbBatchLocationRecords.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    batchId = json['batch_id'];
    skuLocationId = json['sku_location_id'];
    containerRunningCodeId = json['container_running_code_id'];
    locationId = json['location_id'];
    batchQty = json['batch_qty'];
    status = json['status'];
    locationLabel = json['location_label'];
    containerRunningCode = json['container_running_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['batch_id'] = this.batchId;
    data['sku_location_id'] = this.skuLocationId;
    data['container_running_code_id'] = this.containerRunningCodeId;
    data['location_id'] = this.locationId;
    data['batch_qty'] = this.batchQty;
    data['status'] = this.status;
    data['location_label'] = this.locationLabel;
    data['container_running_code'] = this.containerRunningCode;
    return data;
  }
}