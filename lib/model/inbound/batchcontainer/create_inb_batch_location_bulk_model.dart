class CreateInbBatchLocationBulk {
  late String ret;
  late CreateInbBatchLocationBulkData data;

  CreateInbBatchLocationBulk({required this.ret, required this.data});

  CreateInbBatchLocationBulk.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? CreateInbBatchLocationBulkData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CreateInbBatchLocationBulkData {
  late List<CreateInbBatchLocationBulkInfo> batchLocation;

  CreateInbBatchLocationBulkData({required this.batchLocation});

  CreateInbBatchLocationBulkData.fromJson(Map<String, dynamic> json) {
    if (json['batchLocation'] != null) {
      batchLocation = <CreateInbBatchLocationBulkInfo>[];
      json['batchLocation'].forEach((v) {
        batchLocation.add(new CreateInbBatchLocationBulkInfo.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.batchLocation != null) {
      data['batchLocation'] =
          this.batchLocation.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CreateInbBatchLocationBulkInfo {
  late int status;
  late int id;
  late String batchId;
  late int skuLocationId;
  late String containerRunningCodeId;
  late String batchQty;
  late int cby;
  late String cdate;

  CreateInbBatchLocationBulkInfo(
      {required this.status,
        required this.id,
        required this.batchId,
        required this.skuLocationId,
        required this.containerRunningCodeId,
        required this.batchQty,
        required this.cby,
        required this.cdate});

  CreateInbBatchLocationBulkInfo.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    id = json['id'];
    batchId = json['batch_id'];
    skuLocationId = json['sku_location_id'];
    containerRunningCodeId = json['container_running_code_id'];
    batchQty = json['batch_qty'];
    cby = json['cby'];
    cdate = json['cdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['id'] = this.id;
    data['batch_id'] = this.batchId;
    data['sku_location_id'] = this.skuLocationId;
    data['container_running_code_id'] = this.containerRunningCodeId;
    data['batch_qty'] = this.batchQty;
    data['cby'] = this.cby;
    data['cdate'] = this.cdate;
    return data;
  }
}