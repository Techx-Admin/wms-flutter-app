class GetInbContainerInfo {
  late String ret;
  late GetInbContainerInfoData data;

  GetInbContainerInfo({required this.ret, required this.data});

  GetInbContainerInfo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbContainerInfoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbContainerInfoData {
  late int totalCount;
  late List<GetInbContainerInfoRecords> records;

  GetInbContainerInfoData({required this.totalCount, required this.records});

  GetInbContainerInfoData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <GetInbContainerInfoRecords>[];
      json['records'].forEach((v) {
        records.add(new GetInbContainerInfoRecords.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbContainerInfoRecords {
  late int id;
  late int companyId;
  late String containerRunningCode;
  late int status;

  GetInbContainerInfoRecords({required this.id, required this.companyId, required this.containerRunningCode, required this.status});

  GetInbContainerInfoRecords.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    containerRunningCode = json['container_running_code'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['container_running_code'] = this.containerRunningCode;
    data['status'] = this.status;
    return data;
  }
}