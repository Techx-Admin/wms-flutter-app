class GetInbRequestList {
  late String ret;
  late GetInbRequestListData data;

  GetInbRequestList({required this.ret, required this.data});

  GetInbRequestList.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbRequestListData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbRequestListData {
  late int totalCount;
  late List<Records> records;

  GetInbRequestListData({required this.totalCount, required this.records});

  GetInbRequestListData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <Records>[];
      json['records'].forEach((v) {
        records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  late int id;
  late String inboundNo;
  late int clientId;
  late String clientInboundNo;
  late int companyId;
  late int toWarehouseId;
  late int inboundQty;
  late int skuQty;
  late String inboundDate;
  late int status;
  late String cby;
  late String mby;
  late String cdate;
  late String mdate;
  late String clientName;

  Records(
      {required this.id,
        required this.inboundNo,
        required this.clientId,
        required this.clientInboundNo,
        required this.companyId,
        required this.toWarehouseId,
        required this.inboundQty,
        required this.skuQty,
        required this.inboundDate,
        required this.status,
        required this.cby,
        required this.mby,
        required this.cdate,
        required this.mdate,
        required this.clientName});

  Records.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    inboundNo = json['inbound_no'];
    clientId = json['client_id'];
    clientInboundNo = json['client_inbound_no'];
    companyId = json['company_id'];
    toWarehouseId = json['to_warehouse_id'];
    inboundQty = json['inbound_qty'];
    skuQty = json['sku_qty'];
    inboundDate = json['inbound_date'];
    status = json['status'];
    cby = json['cby'];
    mby = json['mby'];
    cdate = json['cdate'];
    mdate = json['mdate'];
    clientName = json['client_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['inbound_no'] = this.inboundNo;
    data['client_id'] = this.clientId;
    data['client_inbound_no'] = this.clientInboundNo;
    data['company_id'] = this.companyId;
    data['to_warehouse_id'] = this.toWarehouseId;
    data['inbound_qty'] = this.inboundQty;
    data['sku_qty'] = this.skuQty;
    data['inbound_date'] = this.inboundDate;
    data['status'] = this.status;
    data['cby'] = this.cby;
    data['mby'] = this.mby;
    data['cdate'] = this.cdate;
    data['mdate'] = this.mdate;
    data['client_name'] = this.clientName;
    return data;
  }
}