class BulkData {
  late String expDate;
  late String qty;

  BulkData(this.expDate, this.qty);

  Map toJson() => {
    'expDate': expDate,
    'qty': qty,
  };
}

class Bulk {
  late String batchId;
  late List<BulkData> bulkData;

  Bulk(this.batchId, this.bulkData);

  Map toJson() => {
    'batchId': batchId,
    'bulkData': bulkData
  };
}