class GetInbBulkSerialNo {
  late String ret;
  late GetInbBulkSerialNoData data;

  GetInbBulkSerialNo({required this.ret, required this.data});

  GetInbBulkSerialNo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbBulkSerialNoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbBulkSerialNoData {
  late int totalCount;
  late List<GetInbBulkSerialNoRecords> records;

  GetInbBulkSerialNoData({required this.totalCount, required this.records});

  GetInbBulkSerialNoData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <GetInbBulkSerialNoRecords>[];
      json['records'].forEach((v) {
        records.add(new GetInbBulkSerialNoRecords.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbBulkSerialNoRecords {
  late int id;
  late int batchId;
  late String serialCode;
  late int qty;
  late int status;

  GetInbBulkSerialNoRecords(
      {required this.id,
        required this.batchId,
        required this.serialCode,
        required this.qty,
        required this.status });

  GetInbBulkSerialNoRecords.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    batchId = json['batch_id'];
    serialCode = json['serial_code'];
    qty = json['qty'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['batch_id'] = this.batchId;
    data['serial_code'] = this.serialCode;
    data['qty'] = this.qty;
    data['status'] = this.status;
    return data;
  }
}
