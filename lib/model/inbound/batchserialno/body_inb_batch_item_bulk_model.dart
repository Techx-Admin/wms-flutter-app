class BulkData {
  late String serialCode;
  late String qty;

  BulkData(this.serialCode, this.qty);

  Map toJson() => {
    'serialCode': serialCode,
    'qty': qty,
  };
}

class Bulk {
  late String batchId;
  late List<BulkData> bulkData;

  Bulk(this.batchId, this.bulkData);

  Map toJson() => {
    'batchId': batchId,
    'bulkData': bulkData
  };
}