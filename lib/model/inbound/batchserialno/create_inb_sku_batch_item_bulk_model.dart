class CreateInbSkuBatchItemBulk {
  late String ret;
  late CreateInbSkuBatchItemBulkList data;

  CreateInbSkuBatchItemBulk({required this.ret, required this.data});

  CreateInbSkuBatchItemBulk.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new CreateInbSkuBatchItemBulkList.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CreateInbSkuBatchItemBulkList {
  late List<CreateInbSkuBatchItemBulkData> skuBatchItem;

  CreateInbSkuBatchItemBulkList({required this.skuBatchItem});

  CreateInbSkuBatchItemBulkList.fromJson(Map<String, dynamic> json) {
    if (json['skuBatchItem'] != null) {
      skuBatchItem = <CreateInbSkuBatchItemBulkData>[];
      json['skuBatchItem'].forEach((v) {
        skuBatchItem.add(new CreateInbSkuBatchItemBulkData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.skuBatchItem != null) {
      data['skuBatchItem'] = this.skuBatchItem.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CreateInbSkuBatchItemBulkData {
  late int status;
  late int id;
  late String serialCode;
  late String batchId;
  late String qty;
  late int cby;
  late String cdate;

  CreateInbSkuBatchItemBulkData(
      {required this.status,
        required this.id,
        required this.serialCode,
        required this.batchId,
        required this.qty,
        required this.cby,
        required this.cdate});

  CreateInbSkuBatchItemBulkData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    id = json['id'];
    serialCode = json['serial_code'];
    batchId = json['batch_id'];
    qty = json['qty'];
    cby = json['cby'];
    cdate = json['cdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['id'] = this.id;
    data['serial_code'] = this.serialCode;
    data['batch_id'] = this.batchId;
    data['qty'] = this.qty;
    data['cby'] = this.cby;
    data['cdate'] = this.cdate;
    return data;
  }
}