class GetInbRequestDispute {
  late String ret;
  late GetInbRequestDisputeData data;

  GetInbRequestDispute({required this.ret, required this.data});

  GetInbRequestDispute.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbRequestDisputeData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbRequestDisputeData {
  late List<InboundRequestDispute> inboundRequestDispute;

  GetInbRequestDisputeData({required this.inboundRequestDispute});

  GetInbRequestDisputeData.fromJson(Map<String, dynamic> json) {
    if (json['inboundRequestDispute'] != null) {
      inboundRequestDispute = <InboundRequestDispute>[];
      json['inboundRequestDispute'].forEach((v) {
        inboundRequestDispute.add(new InboundRequestDispute.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.inboundRequestDispute != null) {
      data['inboundRequestDispute'] =
          this.inboundRequestDispute.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InboundRequestDispute {
  late int id;
  late int requestId;
  late int requestItemId;
  late int oriAmt;
  late int disputeAmt;
  late String reason;
  late int status;
  late String cdate;
  late InboundRequestDisputeItem inboundRequestItem;

  InboundRequestDispute(
      {required this.id,
        required this.requestId,
        required this.requestItemId,
        required this.oriAmt,
        required this.disputeAmt,
        required this.reason,
        required this.status,
        required this.cdate,
        required this.inboundRequestItem});

  InboundRequestDispute.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    requestId = json['request_id'];
    requestItemId = json['request_item_id'];
    oriAmt = json['ori_amt'];
    disputeAmt = json['dispute_amt'];
    reason = json['reason'];
    status = json['status'];
    cdate = json['cdate'];
    inboundRequestItem = (json['inbound_request_item'] != null
        ? InboundRequestDisputeItem.fromJson(json['inbound_request_item'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['request_id'] = this.requestId;
    data['request_item_id'] = this.requestItemId;
    data['ori_amt'] = this.oriAmt;
    data['dispute_amt'] = this.disputeAmt;
    data['reason'] = this.reason;
    data['status'] = this.status;
    data['cdate'] = this.cdate;
    if (this.inboundRequestItem != null) {
      data['inbound_request_item'] = this.inboundRequestItem.toJson();
    }
    return data;
  }
}

class InboundRequestDisputeItem {
  late int id;
  late int requestId;
  late int skuId;
  late int qty;
  late int status;
  late InboundRequestDisputeSku sku;

  InboundRequestDisputeItem(
      {required this.id,
        required this.requestId,
        required this.skuId,
        required this.qty,
        required this.status,
        required this.sku});

  InboundRequestDisputeItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    requestId = json['request_id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    status = json['status'];
    sku = (json['sku'] != null ? InboundRequestDisputeSku.fromJson(json['sku']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['request_id'] = this.requestId;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    if (this.sku != null) {
      data['sku'] = this.sku.toJson();
    }
    return data;
  }
}

class InboundRequestDisputeSku {
  late int id;
  late String skuCode;
  late String skuName;
  late String barcode1;
  late int companyId;
  late int clientId;
  late int status;
  late String remarks;

  InboundRequestDisputeSku(
      {required this.id,
        required this.skuCode,
        required this.skuName,
        required this.barcode1,
        required this.companyId,
        required this.clientId,
        required this.status,
        required this.remarks, });

  InboundRequestDisputeSku.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuCode = json['sku_code'];
    skuName = json['sku_name'];
    barcode1 = json['barcode_1'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    status = json['status'];
    remarks = json['remarks'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_code'] = this.skuCode;
    data['sku_name'] = this.skuName;
    data['barcode_1'] = this.barcode1;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['status'] = this.status;
    data['remarks'] = this.remarks;
    return data;
  }
}
