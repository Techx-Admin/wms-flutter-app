class BulkData {
  late String requestDisputeId;
  late String reason;

  BulkData(this.requestDisputeId, this.reason);

  Map toJson() => {
    'requestDisputeId': requestDisputeId,
    'reason': reason,
    'status': '1',
  };
}

class Bulk {
  late List<BulkData> bulkData;

  Bulk(this.bulkData);

  Map toJson() => {
    'bulkData': bulkData
  };
}