class GetInbDriverList {
  late String ret;
  late GetInbDriverListData data;

  GetInbDriverList({required this.ret, required this.data});

  GetInbDriverList.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbDriverListData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbDriverListData {
  late List<InboundRequestDelivery> inboundRequestDelivery;

  GetInbDriverListData({required this.inboundRequestDelivery});

  GetInbDriverListData.fromJson(Map<String, dynamic> json) {
    if (json['inboundRequestDelivery'] != null) {
      inboundRequestDelivery = <InboundRequestDelivery>[];
      json['inboundRequestDelivery'].forEach((v) {
        inboundRequestDelivery.add(new InboundRequestDelivery.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.inboundRequestDelivery != null) {
      data['inboundRequestDelivery'] =
          this.inboundRequestDelivery.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InboundRequestDelivery {
  late int id;
  late int requestId;
  late String driverName;
  late String driverIcNo;
  late String driverContact;
  late String carPlateNo;
  late String carParkAt;
  late int status;
  late String cdate;

  InboundRequestDelivery(
      {required this.id,
        required this.requestId,
        required this.driverName,
        required this.driverIcNo,
        required this.driverContact,
        required this.carPlateNo,
        required this.carParkAt,
        required this.status,
        required this.cdate });

  InboundRequestDelivery.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    requestId = json['request_id'];
    driverName = json['driver_name'];
    driverIcNo = json['driver_ic_no'];
    driverContact = json['driver_contact'];
    carPlateNo = json['car_plate_no'];
    carParkAt = json['car_park_at'];
    status = json['status'];
    cdate = json['cdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['request_id'] = this.requestId;
    data['driver_name'] = this.driverName;
    data['driver_ic_no'] = this.driverIcNo;
    data['driver_contact'] = this.driverContact;
    data['car_plate_no'] = this.carPlateNo;
    data['car_park_at'] = this.carParkAt;
    data['status'] = this.status;
    data['cdate'] = this.cdate;
    return data;
  }
}