class UpdateInbDriverInfo {
  late String ret;
  late UpdateInbDriverInfoData data;

  UpdateInbDriverInfo({required this.ret, required this.data});

  UpdateInbDriverInfo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? new UpdateInbDriverInfoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UpdateInbDriverInfoData {
  late int updatedRow;

  UpdateInbDriverInfoData({required this.updatedRow});

  UpdateInbDriverInfoData.fromJson(Map<String, dynamic> json) {
    updatedRow = json['updatedRow'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['updatedRow'] = this.updatedRow;
    return data;
  }
}