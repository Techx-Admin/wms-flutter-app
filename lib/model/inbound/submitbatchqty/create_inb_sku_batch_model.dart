class CreateInbSkuBatch {
  late String ret;
  late CreateInbSkuBatchData data;

  CreateInbSkuBatch({required this.ret, required this.data});

  CreateInbSkuBatch.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? CreateInbSkuBatchData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CreateInbSkuBatchData {
  late CreateInbSkuBatchInfo skuBatch;

  CreateInbSkuBatchData({required this.skuBatch});

  CreateInbSkuBatchData.fromJson(Map<String, dynamic> json) {
    skuBatch = (json['skuBatch'] != null
        ? CreateInbSkuBatchInfo.fromJson(json['skuBatch'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.skuBatch != null) {
      data['skuBatch'] = this.skuBatch.toJson();
    }
    return data;
  }
}

class CreateInbSkuBatchInfo {
  late int status;
  late int id;
  late String batchCode;
  late String reqItemId;
  late int skuId;
  late String qty;
  late int cby;
  late String cdate;

  CreateInbSkuBatchInfo(
      {required this.status,
        required this.id,
        required this.batchCode,
        required this.reqItemId,
        required this.skuId,
        required this.qty,
        required this.cby,
        required this.cdate});

  CreateInbSkuBatchInfo.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    id = json['id'];
    batchCode = json['batch_code'];
    reqItemId = json['req_item_id'];
    skuId = json['sku_id'];
    qty = json['qty'];
    cby = json['cby'];
    cdate = json['cdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['id'] = this.id;
    data['batch_code'] = this.batchCode;
    data['req_item_id'] = this.reqItemId;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['cby'] = this.cby;
    data['cdate'] = this.cdate;
    return data;
  }
}