class GetInbSkuBatch {
  late String ret;
  late GetInbSkuBatchData data;

  GetInbSkuBatch({required this.ret, required this.data});

  GetInbSkuBatch.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbSkuBatchData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbSkuBatchData {
  late int totalCount;
  late List<GetInbSkuBatchRecords> records;

  GetInbSkuBatchData({required this.totalCount, required this.records});

  GetInbSkuBatchData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <GetInbSkuBatchRecords>[];
      json['records'].forEach((v) {
        records.add(new GetInbSkuBatchRecords.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbSkuBatchRecords {
  late int id;
  late int reqItemId;
  late String batchCode;
  late int skuId;
  late int qty;
  late int status;

  GetInbSkuBatchRecords(
      {required this.id,
        required this.reqItemId,
        required this.batchCode,
        required this.skuId,
        required this.qty,
        required this.status });

  GetInbSkuBatchRecords.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    reqItemId = json['req_item_id'];
    batchCode = json['batch_code'];
    skuId = json['sku_id'];
    qty = json['qty'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['req_item_id'] = this.reqItemId;
    data['batch_code'] = this.batchCode;
    data['sku_id'] = this.skuId;
    data['qty'] = this.qty;
    data['status'] = this.status;
    return data;
  }
}