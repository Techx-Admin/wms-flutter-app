class GetInbSkuInfo {
  late String ret;
  late GetInbSkuInfoData data;

  GetInbSkuInfo({required this.ret, required this.data});

  GetInbSkuInfo.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbSkuInfoData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbSkuInfoData {
  late int totalCount;
  late List<Records> records;

  GetInbSkuInfoData({required this.totalCount, required this.records});

  GetInbSkuInfoData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <Records>[];
      json['records'].forEach((v) {
        records.add(new Records.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Records {
  late int id;
  late int companyId;
  late int clientId;
  late String skuCode;
  late String skuName;
  late String desc;
  late String barcode1;
  late String barcode2;
  late int categoryId;
  late String category;
  late String skuGroup;
  late String skuSubGroup;
  late int totalQty;
  late int availableQty;
  late String size;
  late String color;
  late String style;
  late String baseUom;
  late String caseUpm;
  late String caseUom;
  late String innerpackUpm;
  late String innerpackUom;
  late int carton;
  late int boxesUpm;
  late int palletUpm;
  late String palletUom;
  late String innerBarcode;
  late String cartonBarcode;
  late String palletBarcode;
  late String costPrice;
  late String sellPrice;
  late String shelfLife;
  late String itemName;
  late String thresholdLow;
  late String thresholdInbound;
  late String duration;
  late int expiry;
  late int serialCode;
  late int status;
  late String cby;
  late String mby;
  late String cdate;
  late String mdate;
  late String remarks;

  Records(
      {required this.id,
        required this.companyId,
        required this.clientId,
        required this.skuCode,
        required this.skuName,
        required this.desc,
        required this.barcode1,
        required this.barcode2,
        required this.categoryId,
        required this.category,
        required this.skuGroup,
        required this.skuSubGroup,
        required this.totalQty,
        required this.availableQty,
        required this.size,
        required this.color,
        required this.style,
        required this.baseUom,
        required this.caseUpm,
        required this.caseUom,
        required this.innerpackUpm,
        required this.innerpackUom,
        required this.carton,
        required this.boxesUpm,
        required this.palletUpm,
        required this.palletUom,
        required this.innerBarcode,
        required this.cartonBarcode,
        required this.palletBarcode,
        required this.costPrice,
        required this.sellPrice,
        required this.shelfLife,
        required this.itemName,
        required this.thresholdLow,
        required this.thresholdInbound,
        required this.duration,
        required this.expiry,
        required this.serialCode,
        required this.status,
        required this.cby,
        required this.mby,
        required this.cdate,
        required this.mdate,
        required this.remarks});

  Records.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    companyId = json['company_id'];
    clientId = json['client_id'];
    skuCode = json['sku_code'];
    skuName = json['sku_name'];
    desc = json['desc'];
    barcode1 = json['barcode_1'];
    barcode2 = json['barcode_2'];
    categoryId = json['category_id'];
    category = json['category'];
    skuGroup = json['sku_group'];
    skuSubGroup = json['sku_sub_group'];
    totalQty = json['total_qty'];
    availableQty = json['available_qty'];
    size = json['size'];
    color = json['color'];
    style = json['style'];
    baseUom = json['base_uom'];
    caseUpm = json['case_upm'];
    caseUom = json['case_uom'];
    innerpackUpm = json['innerpack_upm'];
    innerpackUom = json['innerpack_uom'];
    carton = json['carton'];
    boxesUpm = json['boxes_upm'];
    palletUpm = json['pallet_upm'];
    palletUom = json['pallet_uom'];
    innerBarcode = json['inner_barcode'];
    cartonBarcode = json['carton_barcode'];
    palletBarcode = json['pallet_barcode'];
    costPrice = json['cost_price'];
    sellPrice = json['sell_price'];
    shelfLife = json['shelf_life'];
    itemName = json['item_name'];
    thresholdLow = json['threshold_low'];
    thresholdInbound = json['threshold_inbound'];
    duration = json['duration'];
    expiry = json['expiry'];
    serialCode = json['serial_code'];
    status = json['status'];
    cby = json['cby'];
    mby = json['mby'];
    cdate = json['cdate'];
    mdate = json['mdate'];
    remarks = json['remarks'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['company_id'] = this.companyId;
    data['client_id'] = this.clientId;
    data['sku_code'] = this.skuCode;
    data['sku_name'] = this.skuName;
    data['desc'] = this.desc;
    data['barcode_1'] = this.barcode1;
    data['barcode_2'] = this.barcode2;
    data['category_id'] = this.categoryId;
    data['category'] = this.category;
    data['sku_group'] = this.skuGroup;
    data['sku_sub_group'] = this.skuSubGroup;
    data['total_qty'] = this.totalQty;
    data['available_qty'] = this.availableQty;
    data['size'] = this.size;
    data['color'] = this.color;
    data['style'] = this.style;
    data['base_uom'] = this.baseUom;
    data['case_upm'] = this.caseUpm;
    data['case_uom'] = this.caseUom;
    data['innerpack_upm'] = this.innerpackUpm;
    data['innerpack_uom'] = this.innerpackUom;
    data['carton'] = this.carton;
    data['boxes_upm'] = this.boxesUpm;
    data['pallet_upm'] = this.palletUpm;
    data['pallet_uom'] = this.palletUom;
    data['inner_barcode'] = this.innerBarcode;
    data['carton_barcode'] = this.cartonBarcode;
    data['pallet_barcode'] = this.palletBarcode;
    data['cost_price'] = this.costPrice;
    data['sell_price'] = this.sellPrice;
    data['shelf_life'] = this.shelfLife;
    data['item_name'] = this.itemName;
    data['threshold_low'] = this.thresholdLow;
    data['threshold_inbound'] = this.thresholdInbound;
    data['duration'] = this.duration;
    data['expiry'] = this.expiry;
    data['serial_code'] = this.serialCode;
    data['status'] = this.status;
    data['cby'] = this.cby;
    data['mby'] = this.mby;
    data['cdate'] = this.cdate;
    data['mdate'] = this.mdate;
    data['remarks'] = this.remarks;
    return data;
  }
}