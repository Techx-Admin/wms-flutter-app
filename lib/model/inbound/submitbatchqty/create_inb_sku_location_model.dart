class CreateInbSkuLocation {
  late String ret;
  late CreateInbSkuLocationData data;

  CreateInbSkuLocation({required this.ret, required this.data});

  CreateInbSkuLocation.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? CreateInbSkuLocationData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class CreateInbSkuLocationData {
  late CreateInbSkuLocationInfo skuLocation;

  CreateInbSkuLocationData({required this.skuLocation});

  CreateInbSkuLocationData.fromJson(Map<String, dynamic> json) {
    skuLocation = (json['skuLocation'] != null
        ? new CreateInbSkuLocationInfo.fromJson(json['skuLocation'])
        : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.skuLocation != null) {
      data['skuLocation'] = this.skuLocation.toJson();
    }
    return data;
  }
}

class CreateInbSkuLocationInfo {
  late int status;
  late int id;
  late String skuLocationCode;
  late String batchId;
  late String locationId;
  late String qty;
  late String mfgDate;
  late String expiryDate;
  late int cby;
  late String cdate;

  CreateInbSkuLocationInfo(
      {required this.status,
        required this.id,
        required this.skuLocationCode,
        required this.batchId,
        required this.locationId,
        required this.qty,
        required this.mfgDate,
        required this.expiryDate,
        required this.cby,
        required this.cdate});

  CreateInbSkuLocationInfo.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    id = json['id'];
    skuLocationCode = json['sku_location_code'];
    batchId = json['batch_id'];
    locationId = json['location_id'];
    qty = json['qty'];
    mfgDate = json['mfg_date'];
    expiryDate = json['expiry_date'];
    cby = json['cby'];
    cdate = json['cdate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['id'] = this.id;
    data['sku_location_code'] = this.skuLocationCode;
    data['batch_id'] = this.batchId;
    data['location_id'] = this.locationId;
    data['qty'] = this.qty;
    data['mfg_date'] = this.mfgDate;
    data['expiry_date'] = this.expiryDate;
    data['cby'] = this.cby;
    data['cdate'] = this.cdate;
    return data;
  }
}