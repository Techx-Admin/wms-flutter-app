class GetInbSkuLocation {
  late String ret;
  late GetInbSkuLocationData data;

  GetInbSkuLocation({required this.ret, required this.data});

  GetInbSkuLocation.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
    data = (json['data'] != null ? GetInbSkuLocationData.fromJson(json['data']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class GetInbSkuLocationData {
  late int totalCount;
  late List<GetInbSkuLocationRecords> records;

  GetInbSkuLocationData({required this.totalCount, required this.records});

  GetInbSkuLocationData.fromJson(Map<String, dynamic> json) {
    totalCount = json['totalCount'];
    if (json['records'] != null) {
      records = <GetInbSkuLocationRecords>[];
      json['records'].forEach((v) {
        records.add(new GetInbSkuLocationRecords.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalCount'] = this.totalCount;
    if (this.records != null) {
      data['records'] = this.records.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetInbSkuLocationRecords {
  late int id;
  late String skuLocationCode;
  late int batchId;
  late int locationId;
  late String mfgDate;
  late String expiryDate;
  late int qty;
  late int status;
  late String locationLabel;

  GetInbSkuLocationRecords(
      {required this.id,
        required this.skuLocationCode,
        required this.batchId,
        required this.locationId,
        required this.mfgDate,
        required this.expiryDate,
        required this.qty,
        required this.status,
        required this.locationLabel});

  GetInbSkuLocationRecords.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    skuLocationCode = json['sku_location_code'];
    batchId = json['batch_id'];
    locationId = json['location_id'];
    mfgDate = json['mfg_date'];
    expiryDate = json['expiry_date'];
    qty = json['qty'];
    status = json['status'];
    locationLabel = json['location_label'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['sku_location_code'] = this.skuLocationCode;
    data['batch_id'] = this.batchId;
    data['location_id'] = this.locationId;
    data['mfg_date'] = this.mfgDate;
    data['expiry_date'] = this.expiryDate;
    data['qty'] = this.qty;
    data['status'] = this.status;
    data['location_label'] = this.locationLabel;
    return data;
  }
}
