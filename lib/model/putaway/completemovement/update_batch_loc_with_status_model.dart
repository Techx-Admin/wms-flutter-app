class UpdateBatchLocationWithStatus {
  late String ret;

  UpdateBatchLocationWithStatus({required this.ret});

  UpdateBatchLocationWithStatus.fromJson(Map<String, dynamic> json) {
    ret = json['ret'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ret'] = this.ret;
    return data;
  }
}