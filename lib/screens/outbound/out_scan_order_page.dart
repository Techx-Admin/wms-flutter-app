import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:need_resume/need_resume.dart';
import 'package:wms_app/bloc/outbound/scan_order_cubit.dart';
import 'package:wms_app/model/outbound/scanorder/get_list_order_code_model.dart';
import 'package:wms_app/model/outbound/scanorder/update_order_status_six_model.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:fluttertoast/fluttertoast.dart';

class OutScanOrderPage extends StatefulWidget {
  String logisticName;

  OutScanOrderPage({Key? key, required this.logisticName}) : super(key: key);

  @override
  _OutScanOrderPageState createState() => _OutScanOrderPageState();
}

class _OutScanOrderPageState extends ResumableState<OutScanOrderPage> {
  late ScanOrderCubit cubit;
  List<String> _listOrderId = [];
  List<String> _listOrderCode = [];
  bool scannedSuccessFlag = false;

  bool isVisibleContentData = true;
  bool isVisibleContentEmpty = false;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getListOfOrderCode(widget.logisticName);
    });
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<ScanOrderCubit>();

    return WillPopScope(
      onWillPop: () async {
        String flagInStr = "";
        if(scannedSuccessFlag){
          flagInStr = "1";
        }
        else{
          flagInStr = "0";
        }
        Navigator.pop(context, flagInStr);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadedScanOrderState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            try {
              var res = json.decode(responseInString);
              GetListOrderCode _res = GetListOrderCode.fromJson(res);
              GetListOrderCodeData _resData = GetListOrderCodeData.fromJson(_res.data.toJson());
              OrderLogisticPartnerData _resPartnerData = OrderLogisticPartnerData.fromJson(_resData.orderLogisticPartnerData.toJson());

              for (int i = 0; i < _resPartnerData.orderData.length; i++) {
                String orderId = _resPartnerData.orderData[i].id.toString();
                String orderCode = _resPartnerData.orderData[i].orderCode;

                _listOrderId.add(orderId);
                _listOrderCode.add(orderCode);
              }
            } on Exception catch (e) {
              print(e.toString());
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,

                        // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_back.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    String flagInStr = "";
                                    if(scannedSuccessFlag){
                                      flagInStr = "1";
                                    }
                                    else{
                                      flagInStr = "0";
                                    }
                                    Navigator.pop(context, flagInStr);
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: Ink(
                                  height: 60,
                                  padding: const EdgeInsets.all(5.0),
                                  decoration: ShapeDecoration(
                                    color: ColorConstants.kThemeColor,
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Image.asset(
                                      'images/icon_topbar_scan.png',
                                      color: Colors.white ,
                                    ),
                                    onPressed: () {
                                      scanQr();
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_menu.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {

                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: const EdgeInsets.only(bottom: 100.0, top: 20.0, left: 20.0, right: 20.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kAssignTrayTopBgColor,
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  'Outbound',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color:
                                      ColorConstants.kAssignTrayHeaderColor,
                                      fontSize: 17,

                                      // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )),

                          // listview
                          Visibility(
                            visible: isVisibleContentData,
                            child: Expanded(
                              child: Container(
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    itemCount: _listOrderCode.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                        Divider(
                                            height: 1,
                                            color: ColorConstants.kAssignTrayLineColor),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Column(
                                        children: [
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 1,
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    padding: new EdgeInsets.only(left: 20.0, right: 20.0),
                                                    margin: const EdgeInsets.only(top: 15.0, bottom: 15.0),
                                                    child: Text(
                                                      _listOrderCode[index],
                                                      style: TextStyle(
                                                          color: ColorConstants.kInbListAdapterTextColor,
                                                          fontSize: 12,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w400),
                                                    )
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ),

                          // empty data
                          Visibility(
                            visible: isVisibleContentEmpty,
                            child: Container(
                                width: MediaQuery.of(context).size.width,
                                height: 200,
                                child: Center(
                                  child: Text(
                                    'No Order Code found',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kInbListAdapterTextColor,
                                        fontSize: 12,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),
                          ),
                        ],
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      bool recordFound = false;
      String keepOrderId = "";
      int listSize = _listOrderCode.length;

      for (int i = 0; i < listSize; i++) {
        String orderId = _listOrderId[i];
        String orderCode = _listOrderCode[i];

        if (orderCode == barcode) {
          recordFound = true;
          keepOrderId = orderId;
          break;
        }
      }

      if (recordFound) {
        final response = await cubit.updateOrderStatusToSix(keepOrderId);
        String responseInString = await response;

        if (responseInString == "") {
          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
        else {
          var res = json.decode(responseInString);
          UpdateOrderStatusSix _res = UpdateOrderStatusSix.fromJson(res);
          if(_res.ret == '0') {
            if(_res.data.updatedRow == 1) {
              showToast("Scanned Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

              int listSize = _listOrderCode.length;
              for (int j = 0; j < listSize; j++) {
                String orderId = _listOrderId[j];

                if (orderId == keepOrderId) {
                  scannedSuccessFlag = true;

                  _listOrderId.removeAt(j);
                  _listOrderCode.removeAt(j);

                  if(_listOrderId.length == 0) {
                    isVisibleContentData = false;
                    isVisibleContentEmpty = true;
                  }
                  break;
                }
              }
            }
            else{
              showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
          }
          else {
            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      } else {
        showToast("Order Code not found", Toast.LENGTH_SHORT,
            ToastGravity.BOTTOM);
      }
    }
  }
}