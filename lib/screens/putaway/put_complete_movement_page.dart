import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/bloc/putaway/complete_movement_cubit.dart';
import 'package:wms_app/model/putaway/completemovement/update_batch_loc_with_status_model.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:need_resume/need_resume.dart';

class PutCompleteMovementPage extends StatefulWidget {
  String batchLocationId, containerCode, location;

  PutCompleteMovementPage({Key? key, required this.batchLocationId, required this.containerCode, required this.location}) : super(key: key);

  @override
  _PutCompleteMovementPageState createState() =>
      _PutCompleteMovementPageState();
}

class _PutCompleteMovementPageState extends ResumableState<PutCompleteMovementPage> {
  late CompleteMovementCubit cubit;
  TextEditingController textContainerCode = TextEditingController();
  TextEditingController textLocationLabel = TextEditingController();
  late ProgressDialog pr;

  @override
  void initState() {
    super.initState();

    textContainerCode.text = widget.containerCode;
    textLocationLabel.text = widget.location;
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    cubit = context.watch<CompleteMovementCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
          listener: (context, state) {},
          bloc: cubit,
          child: Scaffold(
            backgroundColor: ColorConstants.kWhiteColor,
            body: SafeArea(
                top: true,
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: Stack(
                    children: <Widget>[

                      // bottom
                      Positioned(
                          bottom: 20.0,
                          right: 0.0,
                          left: 0.0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60,

                            // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Ink(
                                      height: 60,
                                      padding: const EdgeInsets.all(5.0),
                                      decoration: ShapeDecoration(
                                        color: ColorConstants.kThemeColor,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Image.asset(
                                          'images/icon_topbar_scan.png',
                                          color: Colors.white ,
                                        ),
                                        onPressed: () {
                                          scanQr();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_menu.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {

                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),

                      // center
                      Align(
                          alignment: Alignment.center,
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: <Widget>[
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    child: Text(
                                      'You Are Moving',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 20.0, top: 10.0),
                                    child: TextField(
                                      controller: textContainerCode,
                                      enabled: false,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextFieldColor,
                                          fontSize: 20,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder()),
                                      textAlign: TextAlign.center,
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      'To',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 20.0, right: 20.0, top: 10.0),
                                    child: TextField(
                                      controller: textLocationLabel,
                                      enabled: false,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextFieldColor,
                                          fontSize: 20,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                      ),
                                      textAlign: TextAlign.center,
                                    )),
                                Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(top: 10.0),
                                    child: Text(
                                      'Scan Container Code To Complete',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    )),
                              ])),

                    ],
                  ),
                )),
          )),
    );
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      if(barcode == widget.containerCode) {
        String responseInString = "";

        final response = await cubit.updateBatchLocationWithStatusOne(widget.batchLocationId, widget.location);
        responseInString = await response;

        if (responseInString == "") {
          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
        else {
          var res = json.decode(responseInString);
          UpdateBatchLocationWithStatus _res = UpdateBatchLocationWithStatus.fromJson(res);
          if (_res.ret == "0") {
            showToast("Container Code had done moving", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

            Navigator.pop(context, widget.batchLocationId);
          } else {
            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      }
    }
  }
}
