import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/src/provider.dart';
import 'package:wms_app/bloc/putaway/complete_movement_cubit.dart';
import 'package:wms_app/bloc/putaway/pending_to_location_cubit.dart';
import 'package:wms_app/model/putaway/pendingtolocation/get_batch_location_model.dart';
import 'package:wms_app/repo/put_repo.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/screens/putaway/put_complete_movement_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:permission_handler/permission_handler.dart';
import 'package:need_resume/need_resume.dart';

class PutPendingToLocationPage extends StatefulWidget {
  @override
  _PutPendingToLocationPageState createState() => _PutPendingToLocationPageState();
}

class _PutPendingToLocationPageState extends ResumableState<PutPendingToLocationPage> {
  late PendingToLocationCubit cubit;

  bool isVisibleTabAssign = true;
  bool isVisibleTabMoving = false;

  bool isVisibleContentAssign = true;
  bool isVisibleContentMoving = false;
  bool isVisibleContentEmpty = false;

  bool isVisibleBtnScan = false;

  int currentChoose = 1;
  int CHOOSE_ASSIGN = 1;
  int CHOOSE_PENDING = 2;

  List<String> _listAssignBatchLocationId = [];
  List<String> _listAssignContainerCode = [];
  List<String> _listAssignQty = [];
  List<String> _listAssignLocationlabel = [];
  List<String> _listAssignStatus = [];

  List<String> _listPendingBatchLocationId = [];
  List<String> _listPendingContainerCode = [];
  List<String> _listPendingQty = [];
  List<String> _listPendingLocationlabel = [];
  List<String> _listPendingStatus = [];

  @override
  void onResume() {
    switch (resume.source) {
      case 'pending_tab_screen':
        removeDataFromList(resume.data.toString());
        break;
    }
  }

  void removeDataFromList(data){
    int listSize = _listPendingBatchLocationId.length;
    for (int j = 0; j < listSize; j++) {
      String listBatchLocationId = _listPendingBatchLocationId[j];

      if (data == listBatchLocationId) {
        _listPendingBatchLocationId.removeAt(j);
        _listPendingContainerCode.removeAt(j);
        _listPendingQty.removeAt(j);
        _listPendingLocationlabel.removeAt(j);
        _listPendingStatus.removeAt(j);

        break;
      }
    }

    setState(() {
      if(_listPendingBatchLocationId.length == 0) {
        isVisibleContentAssign = false;
        isVisibleContentMoving = false;
        isVisibleContentEmpty = true;
      }
      else {
        isVisibleContentAssign = false;
        isVisibleContentMoving = true;
        isVisibleContentEmpty = false;
      }
    });
  }

  @override
  void initState() {
    super.initState();

    currentChoose = CHOOSE_ASSIGN;

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getBatchWithoutLocation();
    });
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<PendingToLocationCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => GenActionPage(),
          ),
              (route) => false,
        );
        return true;
      },
      child: BlocListener(
          listener: (context, state) {
            if (state is LoadedPendingToLocationState && state.words != null) {
              String responseInString = state.words;

              var responseDecode = json.decode(responseInString);

              GetBatchLocation responseInFull = GetBatchLocation.fromJson(responseDecode);
              GetBatchLocationData _responseData = GetBatchLocationData.fromJson(responseInFull.data.toJson());

              for (int i = 0; i < _responseData.records.length; i++) {
                String batchLocationId = _responseData.records[i].id.toString();
                String containerCode = _responseData.records[i].containerRunningCode;
                String qty = _responseData.records[i].batchQty.toString();
                String locationLabel = _responseData.records[i].locationLabel;
                int status = _responseData.records[i].status;

                if (status == 0) {
                  _listAssignBatchLocationId.add(batchLocationId);
                  _listAssignContainerCode.add(containerCode);
                  _listAssignQty.add(qty);
                  _listAssignLocationlabel.add('Pending');
                  _listAssignStatus.add('0');
                }
              }
            }
            if (state is ReadyPendingToLocationState){
              setState(() {
                if(_listAssignBatchLocationId.length == 0) {
                  isVisibleContentAssign = false;
                  isVisibleContentMoving = false;
                  isVisibleContentEmpty = true;
                }
              });
            }
          },
          bloc: cubit,
          child: Scaffold(
            backgroundColor: ColorConstants.kWhiteColor,
            body: SafeArea(
                top: true,
                child: Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  child: Stack(
                    children: <Widget>[

                      // top
                      Column(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),

                            // https://newbedev.com/the-equivalent-of-wrap-content-and-match-parent-in-flutter
                            child: Wrap(
                                children: [

                                  // choose 'Assign Location'
                                  Visibility(
                                    visible: isVisibleTabAssign,
                                    child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(30.0),
                                              bottomLeft: Radius.circular(30.0),

                                              bottomRight: Radius.circular(30.0),
                                              topRight: Radius.circular(30.0)),
                                          color: ColorConstants.kPendingLocationTabBgColor,
                                        ),
                                        height: 50,
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: (){
                                                  // not respond
                                                },
                                                child: Container(
                                                  child: Center(
                                                    child: Text(
                                                      'Assign Location',
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kPendingLocationTabActiveText,
                                                          fontSize: 12,

                                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(30.0),
                                                        bottomLeft: Radius.circular(30.0),

                                                        bottomRight: Radius.circular(30.0),
                                                        topRight: Radius.circular(30.0)),
                                                    color: ColorConstants.kThemeColor,
                                                  ),
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: (){

                                                  currentChoose = CHOOSE_PENDING;

                                                  refreshList();
                                                },
                                                child: Container(
                                                  child: Center(
                                                    child: Text(
                                                      'Pending Moving',
                                                      textAlign: TextAlign.end,
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kPendingLocationTabInactiveText,
                                                          fontSize: 12,

                                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(30.0),
                                                        bottomLeft: Radius.circular(30.0),

                                                        bottomRight: Radius.circular(30.0),
                                                        topRight: Radius.circular(30.0)),
                                                    color: ColorConstants.kPendingLocationTabBgColor,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                  ),

                                  // choose 'Pending Moving'
                                  Visibility(
                                    visible: isVisibleTabMoving,
                                    child: Container(
                                        width: MediaQuery.of(context).size.width,
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.only(
                                              topLeft: Radius.circular(30.0),
                                              bottomLeft: Radius.circular(30.0),

                                              bottomRight: Radius.circular(30.0),
                                              topRight: Radius.circular(30.0)),
                                          color: ColorConstants.kPendingLocationTabBgColor,
                                        ),
                                        height: 50,
                                        child: Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: (){

                                                  currentChoose = CHOOSE_ASSIGN;

                                                  refreshList();
                                                },
                                                child: Container(
                                                  child: Center(
                                                    child: Text(
                                                      'Assign Location',
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kPendingLocationTabInactiveText,
                                                          fontSize: 12,

                                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(30.0),
                                                        bottomLeft: Radius.circular(30.0),

                                                        bottomRight: Radius.circular(30.0),
                                                        topRight: Radius.circular(30.0)),
                                                    color: ColorConstants.kPendingLocationTabBgColor,
                                                  ),
                                                ),
                                              ),
                                            ),

                                            Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: (){
                                                  // not respond
                                                },
                                                child: Container(
                                                  child: Center(
                                                    child: Text(
                                                      'Pending Moving',
                                                      textAlign: TextAlign.end,
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kPendingLocationTabActiveText,
                                                          fontSize: 12,

                                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w600),
                                                    ),
                                                  ),
                                                  decoration: BoxDecoration(
                                                    borderRadius: BorderRadius.only(
                                                        topLeft: Radius.circular(30.0),
                                                        bottomLeft: Radius.circular(30.0),

                                                        bottomRight: Radius.circular(30.0),
                                                        topRight: Radius.circular(30.0)),
                                                    color: ColorConstants.kThemeColor,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        )
                                    ),
                                  ),

                                ],
                              // ),
                            ),
                          ),

                        ],
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        child: Card(
                          margin: const EdgeInsets.only(bottom: 100.0, top: 90.0, left: 20.0, right: 20.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                          ),
                          elevation: 10,
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              // page header
                              Container(
                                  width: MediaQuery.of(context).size.width,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10.0),
                                        topRight: Radius.circular(10.0)),
                                    color: ColorConstants.kPendingLocationTopBgColor,
                                  ),
                                  height: 50,
                                  child: Center(
                                    child: Text(
                                      'Put-Away',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kAssignTrayHeaderColor,
                                          fontSize: 17,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w400),
                                    ),
                                  )),

                              // choose 'Assign Location'
                              Visibility(
                                visible: isVisibleContentAssign,
                                child: Expanded(
                                  child: Container(
                                    child: ListView.separated(
                                        shrinkWrap: true,
                                        itemCount: _listAssignBatchLocationId.length,
                                        separatorBuilder:
                                            (BuildContext context, int index) =>
                                            Divider(
                                                height: 1,
                                                color: ColorConstants.kAssignTrayLineColor),
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: [
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 20,
                                                    child: Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        margin: const EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
                                                        child: Text(
                                                          _listAssignContainerCode[index],
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kInbListAdapterTextColor,
                                                              fontSize: 12,
                                                              fontFamily: 'Montserrat',
                                                              fontWeight:
                                                              FontWeight.w400),
                                                        )
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 7,
                                                    child: Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                                                        child: Text(
                                                          _listAssignQty[index],
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kInbListAdapterTextColor,
                                                              fontSize: 12,
                                                              fontFamily:
                                                              'Montserrat',
                                                              fontWeight:
                                                              FontWeight.w400),
                                                        )),
                                                  ),
                                                  Expanded(
                                                    flex: 20,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        _displayDialog(context, index);
                                                      },
                                                      child: Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(top: 10.0, right: 20.0, bottom: 10.0),
                                                          child: Text(
                                                            _listAssignLocationlabel[index],
                                                            textAlign: TextAlign.right,
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                'Montserrat',
                                                                fontWeight:
                                                                FontWeight.w400),
                                                          )),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          );
                                        }),
                                  ),
                                ),
                              ),

                              // choose 'Pending Moving'
                              Visibility(
                                visible: isVisibleContentMoving,
                                child: Expanded(
                                  child: Container(
                                    child: ListView.separated(
                                        shrinkWrap: true,
                                        itemCount: _listPendingBatchLocationId.length,
                                        separatorBuilder:
                                            (BuildContext context, int index) =>
                                            Divider(
                                                height: 1,
                                                color: ColorConstants.kAssignTrayLineColor),
                                        itemBuilder:
                                            (BuildContext context, int index) {
                                          return Column(
                                            children: [
                                              Row(
                                                children: <Widget>[
                                                  Expanded(
                                                    flex: 20,
                                                    child: Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        margin: const EdgeInsets.only(left: 20.0, top: 10.0, bottom: 10.0),
                                                        child: Text(
                                                          _listPendingContainerCode[index],
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kInbListAdapterTextColor,
                                                              fontSize: 12,
                                                              fontFamily: 'Montserrat',
                                                              fontWeight:
                                                              FontWeight.w400),
                                                        )
                                                    ),
                                                  ),
                                                  Expanded(
                                                    flex: 7,
                                                    child: Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        margin: const EdgeInsets.only(top: 10.0, bottom: 10.0),
                                                        child: Text(
                                                          _listPendingQty[index],
                                                          textAlign: TextAlign.center,
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kInbListAdapterTextColor,
                                                              fontSize: 12,
                                                              fontFamily:
                                                              'Montserrat',
                                                              fontWeight:
                                                              FontWeight.w400),
                                                        )),
                                                  ),
                                                  Expanded(
                                                    flex: 20,
                                                    child: Container(
                                                        width: MediaQuery.of(context).size.width,
                                                        margin: const EdgeInsets.only(top: 10.0, right: 20.0, bottom: 10.0),
                                                        child: Text(
                                                          _listPendingLocationlabel[index],
                                                          textAlign: TextAlign.right,
                                                          style: TextStyle(
                                                              color: ColorConstants
                                                                  .kInbListAdapterTextColor,
                                                              fontSize: 12,
                                                              fontFamily:
                                                              'Montserrat',
                                                              fontWeight:
                                                              FontWeight.w600),
                                                        )),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          );
                                        }),
                                  ),
                                ),
                              ),

                              // empty data
                              Visibility(
                                visible: isVisibleContentEmpty,
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: 200,
                                    child: Center(
                                      child: Text(
                                        'No Container Code found',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 12,

                                            // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ),

                      // bottom
                      Positioned(
                          bottom: 20.0,
                          right: 0.0,
                          left: 0.0,
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60,

                            // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                            child: Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.pushAndRemoveUntil(
                                          context,
                                          MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                GenActionPage(),
                                          ),
                                              (route) => false,
                                        );
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Visibility(
                                    visible: isVisibleBtnScan,
                                    child: Container(
                                      child: Ink(
                                        height: 60,
                                        padding: const EdgeInsets.all(5.0),
                                        decoration: ShapeDecoration(
                                          color: ColorConstants.kThemeColor,
                                          shape: CircleBorder(),
                                        ),
                                        child: IconButton(
                                          icon: Image.asset(
                                            'images/icon_topbar_scan.png',
                                            color: Colors.white ,
                                          ),
                                          onPressed: () {
                                            scanQrForPending();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_menu.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {

                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          )),
                    ],
                  ),
                )),
          )),
    );
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future scanQrForAssign(int index) async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      final response = await cubit.updateBatchLocationWithStatusTwo(
          _listAssignBatchLocationId[index], barcode
      );
      String responseInString = await response;

      if (responseInString == "") {
        showToast("Please add valid location", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        showToast("Assign location successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

        _listAssignBatchLocationId.removeAt(index);
        _listAssignContainerCode.removeAt(index);
        _listAssignQty.removeAt(index);
        _listAssignLocationlabel.removeAt(index);
        _listAssignStatus.removeAt(index);

        setState(() {
          if(_listAssignBatchLocationId.length == 0) {
            isVisibleContentAssign = false;
            isVisibleContentMoving = false;
            isVisibleContentEmpty = true;
          }
          else {
            isVisibleContentAssign = false;
            isVisibleContentMoving = true;
            isVisibleContentEmpty = false;
          }
        });
      }
    }
  }

  Future scanQrForPending() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      int listSize = _listPendingBatchLocationId.length;

      for (int j = 0; j < listSize; j++) {
        String listContainerCode = _listPendingContainerCode[j];

        if (listContainerCode == barcode ) {

          final response = await cubit.updateBatchLocationWithStatusThree(
              _listPendingBatchLocationId[j], _listPendingLocationlabel[j]
          );
          String responseInString = await response;


          push(
              context,
              MaterialPageRoute(
                  builder: (context) => BlocProvider(
                    create: (context) => CompleteMovementCubit(PutRepository()),
                    child: PutCompleteMovementPage(
                        batchLocationId: _listPendingBatchLocationId[j],
                        containerCode: listContainerCode,
                        location: _listPendingLocationlabel[j]),
                  )), 'pending_tab_screen'
          );

          break;
        }
      }
    }
  }

  Future<void> refreshList() async {
    _listAssignBatchLocationId.clear();
    _listAssignContainerCode.clear();
    _listAssignQty.clear();
    _listAssignLocationlabel.clear();
    _listAssignStatus.clear();

    _listPendingBatchLocationId.clear();
    _listPendingContainerCode.clear();
    _listPendingQty.clear();
    _listPendingLocationlabel.clear();
    _listPendingStatus.clear();

    String responseInString = "";
    final response = await cubit.getBatchWithoutLocationWithoutEmit();
    responseInString = await response;

    var responseDecode = json.decode(responseInString);

    GetBatchLocation responseInFull = GetBatchLocation.fromJson(responseDecode);
    GetBatchLocationData _responseData = GetBatchLocationData.fromJson(responseInFull.data.toJson());

    for (int i = 0; i < _responseData.records.length; i++) {
      String batchLocationId = _responseData.records[i].id.toString();
      String containerCode = _responseData.records[i].containerRunningCode;
      String qty = _responseData.records[i].batchQty.toString();
      String locationLabel = _responseData.records[i].locationLabel;
      int status = _responseData.records[i].status;

      if (currentChoose == CHOOSE_ASSIGN && status == 0) {

        _listAssignBatchLocationId.add(batchLocationId);
        _listAssignContainerCode.add(containerCode);
        _listAssignQty.add(qty);
        _listAssignLocationlabel.add('Pending');
        _listAssignStatus.add('0');

      } else if (currentChoose == CHOOSE_PENDING &&
          ( status == 2 || status == 3 )) {

        _listPendingBatchLocationId.add(batchLocationId);
        _listPendingContainerCode.add(containerCode);
        _listPendingQty.add(qty);
        _listPendingLocationlabel.add(locationLabel);
        _listPendingStatus.add(status.toString());

      }
    }

    if (currentChoose == CHOOSE_ASSIGN) {
      setState(() {
        isVisibleTabAssign = true;
        isVisibleTabMoving = false;

        isVisibleBtnScan = false;

        if(_listAssignBatchLocationId.length == 0) {
          isVisibleContentAssign = false;
          isVisibleContentMoving = false;
          isVisibleContentEmpty = true;
        }
        else {
          isVisibleContentAssign = true;
          isVisibleContentMoving = false;
          isVisibleContentEmpty = false;
        }
      });
    }

    if (currentChoose == CHOOSE_PENDING) {
      isVisibleTabAssign = false;
      isVisibleTabMoving = true;

      isVisibleBtnScan = true;

      setState(() {
        if(_listPendingBatchLocationId.length == 0) {
          isVisibleContentAssign = false;
          isVisibleContentMoving = false;
          isVisibleContentEmpty = true;
        }
        else {
          isVisibleContentAssign = false;
          isVisibleContentMoving = true;
          isVisibleContentEmpty = false;
        }
      });
    }
  }

  Future<void> _displayDialog(BuildContext context, int index) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Add a Location !'),
            actions: <Widget>[
              ElevatedButton(
                child: Text('Cancel'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              ElevatedButton(
                child: Text('Scan'),
                onPressed: () {
                  Navigator.pop(context);
                  scanQrForAssign(index);
                },
              ),
            ],
          );
        });
  }
}
