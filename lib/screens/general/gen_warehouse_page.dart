import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:wms_app/model/general/warehouse_model.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:wms_app/utils/string_utils.dart';
import 'package:wms_app/bloc/general/warehouse_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shimmer/shimmer.dart';

class GenWarehousePage extends StatefulWidget {
  GenWarehousePage({Key? key}) : super(key: key);

  @override
  _GenWarehousePageState createState() => _GenWarehousePageState();
}

class _GenWarehousePageState extends State<GenWarehousePage> {
  List<Records> _listRecords = [];
  late WarehouseCubit cubit;

  @override
  void initState() {
    super.initState();

    // https://stackoverflow.com/questions/49466556/flutter-run-method-on-widget-build-complete
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getListWarehouse();
    });
  }

  getBeforeWidget() {
    return SafeArea(
      top: true,

      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Column(children: [
          Container(
              width: MediaQuery.of(context).size.width,
              margin: const EdgeInsets.only(left: 40.0, top: 40.0),
              child: Text(
                StringConstants.warehouse_text_warehouse,
                style: TextStyle(
                    color: ColorConstants.kPrimaryColor,
                    fontSize: 30,
                    fontFamily: 'Montserrat',
                    fontWeight: FontWeight.w600),
              )),
        ]),
      ),
    );
  }

  getShimmerWidget(BuildContext context) {
    bool _enabled = true;

    return SafeArea(
      top: true,
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Column(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 40.0, top: 40.0),
                child: Text(
                  StringConstants.warehouse_text_warehouse,
                  style: TextStyle(
                      color: ColorConstants.kPrimaryColor,
                      fontSize: 30,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600),
                )),
            Expanded(
              child: Container(
                margin:
                    const EdgeInsets.only(left: 60.0, top: 40.0, right: 60.0),
                child: Shimmer.fromColors(
                  baseColor: Colors.grey[300]!,
                  highlightColor: Colors.grey[100]!,
                  enabled: _enabled,
                  child: ListView.builder(
                    itemCount: 6,
                    itemBuilder: (_, __) => Padding(
                      padding: const EdgeInsets.only(bottom: 35.0),
                      child: Column(
                        // crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(25.0),
                              color: Colors.white,
                            ),
                            height: 50,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getLoadedData() {
    return SafeArea(
      top: true,
      child: Container(
        // https://stackoverflow.com/questions/54988368/flutter-how-to-get-container-with-children-to-take-up-entire-screen
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Column(
          children: [

            // header
            Container(
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.only(left: 40.0, top: 40.0),
                child: Text(
                  StringConstants.warehouse_text_warehouse,
                  style: TextStyle(
                      color: ColorConstants.kPrimaryColor,
                      fontSize: 30,
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.w600),
                )),

            //listview data
            Expanded(
              child: Container(
                margin: const EdgeInsets.only(top: 40.0),
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: _listRecords.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(left: 60.0, right: 60.0, bottom: 35),
                              child: new ElevatedButton(
                                child: Text(
                                  _listRecords[index].name,
                                  style: TextStyle(
                                      color: ColorConstants
                                          .kWarehouseButtonTextColor,
                                      fontSize: 15,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => GenActionPage()));
                                },
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorConstants.kWarehouseButtonBgColor),
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.only(top: 13, bottom: 13)),
                                ),
                              ))
                        ],
                      );
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }

  getErrorWidget(BuildContext context) {
    return SafeArea(
      top: true,
      child: Container(
        // https://stackoverflow.com/questions/54988368/flutter-how-to-get-container-with-children-to-take-up-entire-screen
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,

        child: Stack(
          children: <Widget>[
            Positioned(
                top: 40.0,
                bottom: 0.0,
                right: 0.0,
                left: 40.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0.0),
                  child: Text(
                    StringConstants.warehouse_text_warehouse,
                    style: TextStyle(
                        color: ColorConstants.kPrimaryColor,
                        fontSize: 30,
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.w600),
                  ),
                )),
            Align(
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(StringConstants.warehouse_text_loading_failed,
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black)),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(
                            left: 80.0, right: 80.0, top: 10.0),
                        child: new ElevatedButton(
                          child: Text(
                            StringConstants.warehouse_text_refresh,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,

                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.w400),
                          ),
                          onPressed: () {
                            // fetchWarehouse();
                            cubit.getListWarehouse();
                          },
                          style: ButtonStyle(
                            shape: MaterialStateProperty.all<
                                RoundedRectangleBorder>(RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0),
                            )),
                            backgroundColor:
                                MaterialStateProperty.all(Colors.green),
                            padding: MaterialStateProperty.all(
                                EdgeInsets.only(top: 10, bottom: 10)),
                          ),
                        )),
                  ],
                ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<WarehouseCubit>();

    return WillPopScope(
      onWillPop: () async => false,
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataState && state.words != null) {
            // show listing
            String responseInString = "";

            setState(() {
              responseInString = state.words;
            });

            var res = json.decode(responseInString);
            Warehouse _resModel = Warehouse.fromJson(res);
            Data dataData = Data.fromJson(_resModel.data.toJson());

            if (dataData.totalCount > 0) {
              setState(() {
                _listRecords = dataData.records;
              });
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
            backgroundColor: ColorConstants.kWhiteColor,
            body:
              cubit.state is LoadingState
                ? getShimmerWidget(context)
                : cubit.state is FailState
                    ? getErrorWidget(context)
                    : cubit.state is BeforeState
                        ? getBeforeWidget()
                        : cubit.state is ReadyState
                          ? getLoadedData()
                          : getLoadedData()

            ),
      ),
    );
  }
}
