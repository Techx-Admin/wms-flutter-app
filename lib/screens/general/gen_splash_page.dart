import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wms_app/bloc/general/login_cubit.dart';
import 'package:wms_app/repo/gen_repo.dart';
import 'package:wms_app/screens/general/gen_login_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// https://stackoverflow.com/questions/43879103/adding-a-splash-screen-to-flutter-apps
class GenSplashPage extends StatefulWidget {
  @override
  _GenSplashPageState createState() => _GenSplashPageState();
}

class _GenSplashPageState extends State<GenSplashPage> {
  PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
    buildSignature: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
    _clearSharedPreference();

    // https://stackoverflow.com/questions/49471063/how-to-run-code-after-some-delay-in-flutter
    Timer(Duration(seconds: 3), () {
      // Navigator.of(context).push(MaterialPageRoute(
      //     builder: (context) => GenLoginPage(
      //         versionName: _packageInfo.version,
      //         versionCode: _packageInfo.buildNumber)));

      Navigator.push(context, MaterialPageRoute(builder: (context) => BlocProvider(
          create: (context) => LoginCubit(GeneralRepository()),
          child: GenLoginPage(
              versionName: _packageInfo.version,
              versionCode: _packageInfo.buildNumber),
        )
      ));
    });
  }

  // _infoTile('App name', _packageInfo.appName),
  // _infoTile('Package name', _packageInfo.packageName),
  // _infoTile('App version', _packageInfo.version),
  // _infoTile('Build number', _packageInfo.buildNumber),
  // _infoTile('Build signature', _packageInfo.buildSignature),
  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  Future<void> _clearSharedPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('token');
    prefs.remove('tokenExp');
    prefs.remove('loginFlag');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorConstants.kThemeColor,
    );
  }
}
