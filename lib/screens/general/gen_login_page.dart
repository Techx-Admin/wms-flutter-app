import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/src/provider.dart';
import 'package:wms_app/bloc/general/login_cubit.dart';
import 'package:wms_app/bloc/general/warehouse_cubit.dart';
import 'package:wms_app/model/general/login_model.dart';
import 'package:wms_app/repo/gen_repo.dart';
import 'package:wms_app/screens/general/gen_warehouse_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wms_app/utils/method_utils.dart';
import 'dart:convert';

import 'package:wms_app/utils/string_utils.dart';

class GenLoginPage extends StatefulWidget {
  String versionName, versionCode;

  GenLoginPage({Key? key, required this.versionName, required this.versionCode}) : super(key: key);

  @override
  _GenLoginPageState createState() => _GenLoginPageState();
}

class _GenLoginPageState extends State<GenLoginPage> {
  TextEditingController textUserName = TextEditingController();
  TextEditingController textPassword = TextEditingController();
  bool _obscureText = true;

  // https://flutterawesome.com/a-light-weight-package-to-show-progress-dialog/
  // https://github.com/fayaz07/progress_dialog
  // https://pub.dev/documentation/progress_dialog/latest/
  late ProgressDialog pr;

  @override
  void initState() {
    super.initState();

    // textUserName.text = "warehouseuser2@gmail.com";
    textUserName.text = "warehouseuser1@gmail.com";
    // textUserName.text = "ralry@gmail.com";
    textPassword.text = "1234qwer";
  }

  // https://flutter-examples.com/check-textfield-text-input-is-empty-or-not/
  checkTextFieldEmptyOrNot(BuildContext context, LoginCubit cubit) {
    String userName, password;
    userName = textUserName.text;
    password = textPassword.text;

    if (userName == '') {
      // showToast("Please Enter your username / phone / email", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      showToast(
          StringConstants.login_toast_please_enter_ur_username_phone_email,
          Toast.LENGTH_SHORT,
          ToastGravity.BOTTOM);
    } else {
      if (password == '') {
        // showToast("Please Enter your password", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        showToast(StringConstants.login_toast_please_enter_ur_password,
            Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      } else {
        _checkInternetConnection(userName, password, cubit);
      }
    }
  }

  Future<void> _checkInternetConnection(userName, password, cubit) async {
    try {
      final response = await InternetAddress.lookup('www.google.com');
      if (response.isNotEmpty) {
        fetchData(userName, Method.generateMd5(password), cubit);
      }
    } on SocketException catch (err) {
      showToast(StringConstants.util_alert_no_connection_msg,
          Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
  }

  Future<void> fetchData(String userName, String password, LoginCubit cubit) async {
    await pr.show();

    String responseInString = "";

    // https://stackoverflow.com/questions/60069369/flutter-how-to-convert-futurestring-to-string
    // start
    final response = await cubit.checkLogin(userName, password);
    responseInString = await response;

    // setState(() {
    //   responseInString = response;
    // });
    // end

    if (response == '') {
      await pr.hide();

      // showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      showToast(StringConstants.login_toast_please_try_again,
          Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      String body = responseInString;
      var bodyDecoded = json.decode(body);

      Login login = Login.fromJson(bodyDecoded);
      Data loginData = Data.fromJson(login.data.toJson());

      String token = loginData.token;
      String tokenExp = loginData.tokenExp.toString();

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('token', token);
      prefs.setString('tokenExp', tokenExp);
      prefs.setBool('loginFlag', true);

      await pr.hide();

      // showToast("Login Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      showToast(StringConstants.login_toast_login_success, Toast.LENGTH_SHORT,
          ToastGravity.BOTTOM);

      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                    create: (context) => WarehouseCubit(GeneralRepository()),
                    child: GenWarehousePage(),
                  )));
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    final cubit = context.watch<LoginCubit>();
    // final cubit1 = BlocProvider.of<LoginCubit>(context).state;

    return Scaffold(
      body: SafeArea(
        top: true,
        child: Container(
          // https://stackoverflow.com/questions/54988368/flutter-how-to-get-container-with-children-to-take-up-entire-screen
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,

          child: Stack(
            children: <Widget>[
              //login wording at top
              Positioned(
                  top: 40.0,
                  bottom: 0.0,
                  right: 0.0,
                  left: 40.0,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 0.0),
                    child: Text(
                      StringConstants.login_text_login,
                      style: TextStyle(
                          color: ColorConstants.kPrimaryColor,
                          fontSize: 30,
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w600),
                    ),
                  )),

              // version at bottom
              Positioned(
                  bottom: 20.0,
                  right: 0.0,
                  left: 0.0,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      // https://www.geeksforgeeks.org/how-to-append-or-concatenate-strings-in-dart/
                      StringConstants.login_text_version +
                          ' ' +
                          widget.versionName +
                          ' (' +
                          widget.versionCode +
                          ')',

                      style: TextStyle(
                          color: ColorConstants.kPrimaryColor,
                          fontSize: 14,

                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.w400),
                    ),
                  )),

              // center
              Align(
                  alignment: Alignment.center,
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        // textfield username
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                            child: TextField(
                              controller: textUserName,
                              decoration: InputDecoration(
                                prefixIcon:
                                    Image.asset('images/icon_gen_username.png'),
                                border: OutlineInputBorder(),
                                labelText: StringConstants
                                    .login_hint_enter_ur_username_phone_email,
                              ),
                            )
                        ),
                        // textfield password
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only(
                                left: 20.0, right: 20.0, top: 10.0),
                            child: TextField(
                              controller: textPassword,
                              obscureText: _obscureText,
                              decoration: InputDecoration(
                                prefixIcon:
                                    Image.asset('images/icon_gen_password.png'),
                                border: OutlineInputBorder(),
                                labelText: StringConstants
                                    .login_hint_enter_ur_password,
                              ),
                            )
                        ),
                        // button login
                        Container(
                            width: MediaQuery.of(context).size.width,
                            margin: const EdgeInsets.only(
                                left: 80.0, right: 80.0, top: 10.0),
                            child: new ElevatedButton(
                              child: Text(
                                'Login',
                                style: TextStyle(
                                    color: ColorConstants.kButtonTextColor,
                                    fontSize: 18,

                                    // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              ),
                              onPressed: () {
                                checkTextFieldEmptyOrNot(context, cubit);
                              },
                              style: ButtonStyle(
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(25.0),
                                )),
                                backgroundColor: MaterialStateProperty.all(
                                    ColorConstants.kButtonBgColor),
                                padding: MaterialStateProperty.all(
                                    EdgeInsets.only(top: 10, bottom: 10)),
                              ),
                            )),
                      ])),
            ],
          ),
        ),
      ),
    );
  }
}
