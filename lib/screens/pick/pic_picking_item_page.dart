import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/bloc/pick/picking_item_cubit.dart';
import 'package:wms_app/model/pick/pickingitem/get_list_pick_item_model.dart';
import 'package:wms_app/model/pick/pickingitem/body_pick_item_location_bulk_model.dart';
import 'package:wms_app/model/pick/pickingitem/update_pick_location_bulk_model.dart';
import 'package:wms_app/model/pick/pickingitem/update_pick_status_model.dart';
import 'package:wms_app/screens/pick/pic_complete_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:wms_app/utils/string_utils.dart';
import 'package:intl/intl.dart';

class PicPickingItemPage extends StatefulWidget {
  String pickId, orderId;

  PicPickingItemPage({
    Key? key,
    required this.pickId,
    required this.orderId,
  }) : super(key: key);

  @override
  _PicPickingItemPageState createState() => _PicPickingItemPageState();
}

class _PicPickingItemPageState extends State<PicPickingItemPage> {
  late PickingItemCubit cubit;
  bool isVisible = true;
  List<String> _listOrderPickItemLocationId = [];
  List<String> _listLocationCurrentItem = [];
  List<String> _listLocationPreviousItem = [];
  List<String> _listLocationActualItem = [];
  List<String> _listAmountItem = [];
  List<String> _listScannedItem = [];
  List<String> _listTrayCode = [];
  List<String> _listOrderItemId = [];
  List<int> _listEditable = [];
  int totalAmount = 0;
  int currentItemIndex = 0, totalItemIndex = 0;
  late ProgressDialog pr;
  String valueAlert = "";

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getOrderPickDetails(widget.pickId);
    });
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<PickingItemCubit>();
    pr = ProgressDialog(context);

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
          listener: (context, state) {
            if (state is LoadedPickingItemState && state.words != null) {
              String responseInString = "";
              setState(() {
                responseInString = state.words;
              });

              var res = json.decode(responseInString);
              GetListPickItem _res = GetListPickItem.fromJson(res);
              GetListPickItemData _resData = GetListPickItemData.fromJson(_res.data.toJson());
              List<List<OrderPickItemLocation>> trayList = _resData.orderPickItemLocation;

              for (int i = 0; i < trayList.length; i++) {
                int locationSize = trayList[i].length;
                for (int j = 0; j < locationSize; j++) {
                  String pickItemLocationId = trayList[i][j].id.toString();

                  String locationCurrentItem = trayList[i][j].locationLabel;
                  int amountItem = trayList[i][j].qty;
                  String trayNo = trayList[i][j].trayId.toString();
                  String trayCode = trayList[i][j].trayCode;
                  String orderItemId = trayList[i][j].orderItemId.toString();

                  totalAmount = totalAmount + amountItem;

                  _listOrderPickItemLocationId.add(pickItemLocationId);
                  _listAmountItem.add(amountItem.toString());
                  _listScannedItem.add("0");
                  _listTrayCode.add(trayCode);
                  _listOrderItemId.add(orderItemId);
                  _listEditable.add(0);

                  String locationPrevItem = "";
                  if (j == 0) {
                    locationPrevItem = "empty";
                  }

                  if (j > 0) {
                    int previousPosition = _listLocationCurrentItem.length - 1;
                    locationPrevItem = _listLocationCurrentItem[previousPosition];
                  }

                  _listLocationCurrentItem.add(locationCurrentItem);
                  if(locationCurrentItem == locationPrevItem) {
                    locationCurrentItem = "";
                  }

                  _listLocationActualItem.add(locationCurrentItem);
                  _listLocationPreviousItem.add(locationPrevItem);
                }
              }
            }
          },
          bloc: cubit,
          child: Scaffold(
            body: SafeArea(
              top: true,
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    // bottom
                    Positioned(
                        bottom: 20.0,
                        right: 0.0,
                        left: 0.0,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 130,

                          // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                          child: Column(
                            children: <Widget>[
                              Visibility(
                                visible: isVisible,
                                maintainSize: true,
                                maintainAnimation: true,
                                maintainState: true,
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 60.0, right: 60.0, bottom: 10.0),
                                    child: new ElevatedButton(
                                      child: Text(
                                        'Submit',
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kGenPicklistButtonTextColor,
                                            fontSize: 15,

                                            // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      onPressed: () {
                                        checkBeforeIntent();
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                            RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(25.0),
                                            )),
                                        backgroundColor:
                                        MaterialStateProperty.all(
                                            ColorConstants.kThemeColor),
                                        padding: MaterialStateProperty.all(
                                            EdgeInsets.only(
                                                top: 10, bottom: 10)),
                                      ),
                                    )),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: IconButton(
                                        iconSize: 20,
                                        padding: const EdgeInsets.all(15.0),
                                        icon: Image.asset(
                                          'images/icon_topbar_back.png',
                                          color: Colors.black,
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop(true);
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: Ink(
                                        height: 60,
                                        padding: const EdgeInsets.all(5.0),
                                        decoration: ShapeDecoration(
                                          color: ColorConstants.kThemeColor,
                                          shape: CircleBorder(),
                                        ),
                                        child: IconButton(
                                          icon: Image.asset(
                                            'images/icon_topbar_scan.png',
                                            color: Colors.white ,
                                          ),
                                          onPressed: () {
                                            scanQr();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: IconButton(
                                        iconSize: 20,
                                        padding: const EdgeInsets.all(15.0),
                                        icon: Image.asset(
                                          'images/icon_topbar_menu.png',
                                          color: Colors.black,
                                        ),
                                        onPressed: () {

                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )),

                    // top
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      // margin: const EdgeInsets.only(bottom: 100.0, top: 20.0),
                      margin: const EdgeInsets.only(bottom: 150.0, top: 20.0),
                      // padding: new EdgeInsets.all(10.0),
                      padding: new EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            // header
                            Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                height: 50,
                                child: Center(
                                  child: Text(
                                    'Picking',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kAssignTrayHeaderColor,
                                        fontSize: 17,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),
                            //listview header
                            Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 25,
                                      child: Container(
                                        margin: const EdgeInsets.only(left: 20.0),
                                        child: Text(
                                          'Location of Item',
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistTextColor,
                                              fontSize: 12,

                                              // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 20,
                                      child: Container(
                                        child: Center(
                                          child: Text(
                                            'Amount',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kGenPicklistTextColor,
                                                fontSize: 12,

                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 15,
                                      child: Container(
                                        child: Center(
                                          child: Text(
                                            'To Tray #',
                                            textAlign: TextAlign.end,
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kGenPicklistTextColor,
                                                fontSize: 12,

                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            //listview data
                            Expanded(
                              child: Container(
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    itemCount: _listOrderPickItemLocationId.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                        Divider(
                                            height: 1,
                                            color: ColorConstants.kAssignTrayLineColor
                                        ),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Column(
                                        children: [
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 25,
                                                child: Container(
                                                    margin: const EdgeInsets.only(top: 10.0, left: 20.0, bottom: 10.0),
                                                    child: Text(
                                                      _listLocationActualItem[index],
                                                      style: TextStyle(
                                                          color: ColorConstants.kAssignTrayTextColor,
                                                          fontSize: 14,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight.w400),
                                                    )),
                                              ),
                                              Expanded(
                                                flex: 15,
                                                child: GestureDetector(
                                                  onTap: () {
                                                    int editable = _listEditable[index];

                                                    if(editable == 1) {
                                                      _displayInputDialog(context, index);
                                                    }
                                                  },
                                                  child: Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      child: Center(
                                                        child: Text(
                                                          _listScannedItem[index] + "/" + _listAmountItem[index],
                                                          style: TextStyle(
                                                              color: ColorConstants.kAssignTrayTextColor,
                                                              fontSize: 14,
                                                              fontFamily: 'Montserrat',
                                                              fontWeight: FontWeight.w400),
                                                        ),
                                                      )),
                                                ),
                                              ),
                                              Expanded(
                                                flex: 5,
                                                child: Container(
                                                  height: 20,
                                                  child: Image.asset(
                                                    "images/icon_all_next_page.png",
                                                    fit: BoxFit.contain,
                                                    width: 20,
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                  flex: 15,
                                                  child: Container(
                                                      width: MediaQuery.of(context).size.width,
                                                      margin: const EdgeInsets.only(top: 10.0, bottom: 10.0, right: 10.0),
                                                      child: Text(
                                                        _listTrayCode[index],
                                                        style: TextStyle(
                                                            color: ColorConstants.kAssignTrayTextColor,
                                                            fontSize: 14,
                                                            fontFamily: 'Montserrat',
                                                            fontWeight: FontWeight.w400),
                                                      ))),
                                            ],
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
          )),
    );
  }

  Future<void> _displayInputDialog(BuildContext context, int index) async {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text('Change Amount !'),
            content: TextField(
              onChanged: (value) {
                setState(() {
                  valueAlert = value;
                });
              },
            ),
            actions: <Widget>[
              ElevatedButton(
                child: Text('Cancel'),
                onPressed: () {
                  setState(() {
                    Navigator.pop(context);
                  });
                },
              ),
              ElevatedButton(
                child: Text('Save'),
                onPressed: () {
                  if(valueAlert.isEmpty) {
                    setState(() {
                      Navigator.pop(context);
                    });
                    showToast("Please enter Amount",
                        Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
                  }
                  else {
                    int totalItem = int.parse(_listAmountItem[index]);
                    int currentItem = int.parse(valueAlert);

                    if(currentItem >= totalItem) {
                      if(currentItem == totalItem) {
                        _listScannedItem[index] = valueAlert;
                        _listEditable[index] = 0;
                        setState(() {
                          Navigator.pop(context);
                        });
                      }
                      else {
                        setState(() {
                          Navigator.pop(context);
                        });
                        showToast("Please enter correct Amount",
                            Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
                      }
                    }
                    else {
                      if(currentItem == 0) {
                        setState(() {
                          Navigator.pop(context);
                        });
                        showToast("Please enter correct Amount",
                            Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
                      } else {
                        _listScannedItem[index] = valueAlert;
                        setState(() {
                          Navigator.pop(context);
                        });
                      }
                    }
                  }
                },
              ),
            ],
          );
        });
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {

      bool result = false;
      int size  = _listOrderPickItemLocationId.length;



      for(int i = 0 ; i < size ; i++){
        String locationCurrentItem = _listLocationCurrentItem[i];

        // https://dev.to/wangonya/how-you-turn-a-string-into-a-number-or-vice-versa-with-dart-392h
        int scannedItem = int.parse(_listScannedItem[i]);
        int totalItem = int.parse(_listAmountItem[i]);

        if(barcode == locationCurrentItem) {
          if(scannedItem == totalItem) {
            // no action
          }
          else {
            scannedItem = scannedItem + 1;

            _listScannedItem[i] = scannedItem.toString();

            // 2021-11-2 if scannedItem more than 1 qty, enable user click to edit
            if(scannedItem == 1) {
              _listEditable[i] = 1;
            }

            // 2021-11-2 if scannedItem equal total, disable edit
            if(scannedItem == totalItem) {
              _listEditable[i] = 0;
            }


            result = true;
            break;
          }
        }
      }

      if(! result){
        showToast("Location of Item not found", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        // Toast.makeText(activity, getString(R.string.pick_toast_items_not_found), Toast.LENGTH_SHORT).show();
      }
      else {
        setState(() {

        });
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }

  Future checkBeforeIntent() async {
    int totalScan = 0;
    int size  = _listOrderPickItemLocationId.length;
    for(int i = 0 ; i < size ; i++){
      // int scannedItem = Integer.parseInt(pickInProgressList.get(i).getScannedItem());
      int scannedItem = int.parse(_listScannedItem[i]);
      totalScan = totalScan + scannedItem;
    }

    if(totalScan == totalAmount) {
      try {
        final response = await InternetAddress.lookup('www.google.com');
        if (response.isNotEmpty) {
          List<Pick> _listPick = [];

          await pr.show();

          for(int i = 0 ; i < size ; i++){
            String orderPickItemLocationId = _listOrderPickItemLocationId[i];
            String scannedItem = _listScannedItem[i];
            _listPick.add(Pick(orderPickItemLocationId, scannedItem, '1'));
          }

          var details = new Map();
          details['bulkData'] = _listPick;

          final response = await cubit.updateOrderPickItemLocationInBulk(details);
          String responseInString = await response;

          var res = json.decode(responseInString);
          try {
            UpdatePickLocationBulk _res = UpdatePickLocationBulk.fromJson(res);
            if (_res.ret == "0") {
              final responseTwo = await cubit.updateOrderPick(widget.pickId);
              String responseTwoInString = await responseTwo;

              var resTwo = json.decode(responseTwoInString);
              UpdatePickStatus _resTwo = UpdatePickStatus.fromJson(resTwo);

              if (_resTwo.ret == "0") {

                currentItemIndex = 0;
                totalItemIndex = _listOrderPickItemLocationId.length - 1;
                if (size > 0) {
                  for(int k = 0 ; k <= totalItemIndex ; k++){
                    String orderItemId = _listOrderItemId[k];

                    DateTime now = DateTime.now();
                    String formattedDate = DateFormat('yyyy-MM-dd').format(now);

                    String responseThreeInString = "";
                    final responseThree = await cubit.updateOrderItemStatus(orderItemId, formattedDate);
                    responseThreeInString = await responseThree;

                    currentItemIndex = k;
                  }

                  if(currentItemIndex == totalItemIndex){
                    var orderList = widget.orderId.split(",");
                    int currentOrderIndex = 0;
                    int totalOrderIndex = orderList.length - 1;

                    int size = orderList.length;
                    if(size > 0){
                      String orderId = orderList[0];

                      final responseFour = await cubit.updateOrderStatus(orderId);
                      String responseFourInString = await responseFour;

                      if(currentOrderIndex == totalOrderIndex){
                        await pr.hide();
                        showToast("Submit Successful", Toast.LENGTH_SHORT,
                            ToastGravity.BOTTOM);

                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PicCompletePage()));
                      }
                      else {
                        currentOrderIndex = currentOrderIndex + 1;
                        for(int m = 1 ; m <= totalOrderIndex ; m++){
                          String orderId = orderList[m];

                          final responseFour = await cubit.updateOrderStatus(orderId);
                          String responseFourInString = await responseFour;
                        }

                        await pr.hide();
                        showToast("Submit Successful", Toast.LENGTH_SHORT,
                            ToastGravity.BOTTOM);

                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => PicCompletePage()));
                      }
                    }
                    else{
                      await pr.hide();
                      showToast("Please try again", Toast.LENGTH_SHORT,
                          ToastGravity.BOTTOM);
                    }
                  }
                  else {
                    await pr.hide();
                    showToast("Please try again", Toast.LENGTH_SHORT,
                        ToastGravity.BOTTOM);
                  }
                }
                else {
                  await pr.hide();
                  showToast("Please try again", Toast.LENGTH_SHORT,
                      ToastGravity.BOTTOM);
                }
              }
              else {
                await pr.hide();
                showToast("Please try again", Toast.LENGTH_SHORT,
                    ToastGravity.BOTTOM);
              }
            }
            else {
              await pr.hide();
              showToast("Please try again", Toast.LENGTH_SHORT,
                  ToastGravity.BOTTOM);
            }
          } catch (e) {
            print('error exception caught: $e');
            await pr.hide();
            showToast("Please try again", Toast.LENGTH_SHORT,
                ToastGravity.BOTTOM);
          }
        }
      } on SocketException catch (err) {
        print('error SocketException caught: $err');

        bool isProgressDialogShowing = pr.isShowing();

        if(isProgressDialogShowing) {
          await pr.hide();
        }

        showToast(StringConstants.util_alert_no_connection_msg, Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
    else{
      showToast("Please complete scan all items before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }

  }
}
