import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wms_app/bloc/pick/assign_tray_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:wms_app/bloc/pick/picking_item_cubit.dart';
import 'package:wms_app/model/pick/assigntray/get_order_pick_detail_model.dart';
import 'package:wms_app/model/pick/assigntray/get_tray_info_model.dart';
import 'package:wms_app/model/pick/assigntray/update_tray_status_model.dart';
import 'package:wms_app/repo/pic_repo.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/screens/pick/pic_picking_item_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:progress_dialog/progress_dialog.dart';

class PicAssignTrayPage extends StatefulWidget {
  String pickId;

  PicAssignTrayPage({Key? key, required this.pickId}) : super(key: key);

  @override
  _PicAssignTrayPageState createState() => _PicAssignTrayPageState();
}

class _PicAssignTrayPageState extends State<PicAssignTrayPage> {
  late AssignTrayCubit cubit;
  List<String> _listNo = [];
  List<String> _listOrderNo = [];
  List<String> _listTrayCode = [];
  List<String> _listPickTrayId = [];
  List<String> _listOrderId = [];
  List<String> _listPreTrayId = [];
  List<String> _listPreTrayCode = [];
  bool isVisible = false;
  String keepTrayId = "";
  String keepOrderId= "";
  late ProgressDialog pr;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getOrderPickDetails(widget.pickId);
    });
  }

  @override
  Widget build(BuildContext context) {
    pr = ProgressDialog(context);
    cubit = context.watch<AssignTrayCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => GenActionPage(),
          ),
          (route) => false,
        );
        return true;
      },
      child: BlocListener(
          listener: (context, state) {
            if (state is LoadedAssignTrayState && state.words != null) {
              String responseInString = "";
              setState(() {
                responseInString = state.words;
              });

              var res = json.decode(responseInString);
              GetOrderPickDetail _res = GetOrderPickDetail.fromJson(res);
              GetOrderPickDetailData _resData =
                  GetOrderPickDetailData.fromJson(_res.data.toJson());
              OrderPickDetails pickDetails =
                  OrderPickDetails.fromJson(_resData.orderPickDetails.toJson());

              for (int i = 0; i < pickDetails.orderPickTray.length; i++) {
                String no = (i + 1).toString();
                String orderNo = pickDetails.orderPickTray[i].order.cusOrderNo;
                String trayId = pickDetails.orderPickTray[i].trayId.toString();
                String trayCode = pickDetails.orderPickTray[i].trayCode;
                String pickTrayId = pickDetails.orderPickTray[i].id.toString();

                String orderId =
                    pickDetails.orderPickTray[i].orderId.toString();
                _listOrderId.add(orderId);

                if (trayId == "null") {
                  trayCode = "?";
                }

                _listNo.add(no);
                _listOrderNo.add(orderNo);
                _listTrayCode.add(trayCode);
                _listPickTrayId.add(pickTrayId);
                _listPreTrayId.add("");
                _listPreTrayCode.add("");
              }

              var distinctOrderId = _listOrderId
                  .toSet()
                  .toList(); // https://stackoverflow.com/questions/12030613/how-can-i-delete-duplicates-in-a-dart-list-list-distinct

              keepOrderId = "";
              int previousIndex = distinctOrderId.length - 1;

              for (int i = 0; i < distinctOrderId.length; i++) {
                keepOrderId = keepOrderId + distinctOrderId[i];
                if (i < previousIndex) {
                  keepOrderId = keepOrderId + ",";
                }
              }

              bool isShowButton = true;
              for (int i = 0; i < _listNo.length; i++) {
                String currentTrayCode = _listTrayCode[i];
                if (currentTrayCode == '?') {
                  isShowButton = false;
                }
              }

              if (isShowButton) {
                setState(() {
                  isVisible = true;
                });
              } else {
                setState(() {
                  isVisible = false;
                });
              }
            }
          },
          bloc: cubit,
          child: Scaffold(
            body: SafeArea(
              top: true,
              child: Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Stack(
                  children: <Widget>[
                    // bottom
                    Positioned(
                        bottom: 20.0,
                        right: 0.0,
                        left: 0.0,
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: 130,

                          // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                          child: Column(
                            children: <Widget>[
                              Visibility(
                                visible: isVisible,
                                maintainSize: true,
                                maintainAnimation: true,
                                maintainState: true,
                                child: Container(
                                    width: MediaQuery.of(context).size.width,
                                    margin: const EdgeInsets.only(
                                        left: 60.0, right: 60.0, bottom: 10.0),
                                    child: new ElevatedButton(
                                      child: Text(
                                        'Next',
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kGenPicklistButtonTextColor,
                                            fontSize: 15,

                                            // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w600),
                                      ),
                                      onPressed: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => BlocProvider(
                                                  create: (context) => PickingItemCubit(PickRepository()),
                                                  child: PicPickingItemPage(pickId: widget.pickId, orderId: keepOrderId),
                                                )));
                                      },
                                      style: ButtonStyle(
                                        shape: MaterialStateProperty.all<
                                                RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(25.0),
                                        )),
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                ColorConstants.kThemeColor),
                                        padding: MaterialStateProperty.all(
                                            EdgeInsets.only(
                                                top: 10, bottom: 10)),
                                      ),
                                    )),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: IconButton(
                                        iconSize: 20,
                                        padding: const EdgeInsets.all(15.0),
                                        icon: Image.asset(
                                          'images/icon_topbar_back.png',
                                          color: Colors.black,
                                        ),
                                        onPressed: () {
                                          Navigator.pushAndRemoveUntil(
                                            context,
                                            MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  GenActionPage(),
                                            ),
                                            (route) => false,
                                          );
                                        },
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: Ink(
                                        height: 60,
                                        padding: const EdgeInsets.all(5.0),
                                        decoration: ShapeDecoration(
                                          color: ColorConstants.kThemeColor,
                                          shape: CircleBorder(),
                                        ),
                                        child: IconButton(
                                          icon: Image.asset(
                                            'images/icon_topbar_scan.png',
                                            color: Colors.white ,),
                                          onPressed: () {
                                            scanQr();
                                          },
                                        ),
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    flex: 1,
                                    child: Container(
                                      child: IconButton(
                                        iconSize: 20,
                                        padding: const EdgeInsets.all(15.0),
                                        icon: Image.asset(
                                          'images/icon_topbar_menu.png',
                                          color: Colors.black,
                                        ),
                                        onPressed: () {

                                        },
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        )),

                    // top
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      // margin: const EdgeInsets.only(bottom: 100.0, top: 20.0),
                      margin: const EdgeInsets.only(bottom: 150.0, top: 20.0),
                      // padding: new EdgeInsets.all(10.0),
                      padding: new EdgeInsets.only(left: 20.0, right: 20.0),
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                height: 50,
                                child: Center(
                                  child: Text(
                                    'Picking',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kAssignTrayHeaderColor,
                                        fontSize: 17,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),
                            Container(
                                height: 50,
                                width: MediaQuery.of(context).size.width,
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(left: 50.0),
                                        child: Text(
                                          '# of Order',
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistTextColor,
                                              fontSize: 12,

                                              // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Container(
                                        margin:
                                            const EdgeInsets.only(right: 50.0),
                                        child: Text(
                                          '# of Tray',
                                          textAlign: TextAlign.end,
                                          style: TextStyle(
                                              color: ColorConstants
                                                  .kGenPicklistTextColor,
                                              fontSize: 12,

                                              // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                              fontFamily: 'Montserrat',
                                              fontWeight: FontWeight.w600),
                                        ),
                                      ),
                                    ),
                                  ],
                                )),
                            Expanded(
                              child: Container(
                                child: ListView.separated(
                                    shrinkWrap: true,
                                    itemCount: _listNo.length,
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            Divider(
                                                height: 1,
                                                color: ColorConstants
                                                    .kAssignTrayLineColor),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return Column(
                                        children: [
                                          Row(
                                            children: <Widget>[
                                              Expanded(
                                                flex: 25,
                                                child: Container(
                                                    child: Center(
                                                  child: Text(
                                                    '#' + _listNo[index],
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kAssignTrayTextColor,
                                                        fontSize: 14,
                                                        fontFamily:
                                                            'Montserrat',
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                )),
                                              ),
                                              Expanded(
                                                flex: 50,
                                                child: Container(
                                                    width:
                                                        MediaQuery.of(context)
                                                            .size
                                                            .width,
                                                    child: Text(
                                                      _listOrderNo[index],
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kAssignTrayTextColor,
                                                          fontSize: 14,
                                                          fontFamily:
                                                              'Montserrat',
                                                          fontWeight:
                                                              FontWeight.w400),
                                                    )),
                                              ),
                                              Expanded(
                                                flex: 25,
                                                child: Container(
                                                  height: 20,
                                                  child: Image.asset(
                                                    "images/icon_all_next_page.png",
                                                    fit: BoxFit.contain,
                                                    width: 20,
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                  flex: 50,
                                                  child: Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      margin:
                                                          const EdgeInsets.only(
                                                              top: 10.0,
                                                              bottom: 10.0),
                                                      child: Text(
                                                        _listTrayCode[index],
                                                        style: TextStyle(
                                                            color: ColorConstants
                                                                .kAssignTrayTextColor,
                                                            fontSize: 14,
                                                            fontFamily:
                                                                'Montserrat',
                                                            fontWeight:
                                                                FontWeight
                                                                    .w400),
                                                      ))),
                                            ],
                                          ),
                                        ],
                                      );
                                    }),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )),
    );
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      await pr.show();
      String responseInString = "";

      final response = await cubit.getTrayInfo(barcode);
      responseInString = await response; // https://www.titanwolf.org/Network/q/81325b77-2214-47ee-8830-86a060b92a41/y

      try {
        var res = json.decode(responseInString);
        GetTrayInfo _resModel = GetTrayInfo.fromJson(res);
        GetTrayInfoData dataData =
            GetTrayInfoData.fromJson(_resModel.data.toJson());
        if (dataData.totalCount == 0) {
          await pr.hide();
          showToast("Please Scan Valid Tray Code", Toast.LENGTH_SHORT,
              ToastGravity.BOTTOM);
        } else {
          // compare scanned tray code equal to api tray code
          // if have same, then compare scanned tray code to current listview tray code
          // if same, show toast for exist, else check listview tray
          // if tray code = ? then check tray status
          // if tray status = 0, update tray code to pretrayid and pretraycode, before update to actual traycode
          // then call api
          for (int i = 0; i < dataData.records.length; i++) {
            String listTrayId = dataData.records[i].id.toString();
            String listTrayCode = dataData.records[i].trayCode;
            int listTrayStatus = dataData.records[i].status;

            if (barcode == listTrayCode) {
              bool existTrayCode = false;
              int size = _listNo.length;
              for (int j = 0; j < size; j++) {
                String trayCode = _listTrayCode[j];

                if (trayCode == barcode) {
                  existTrayCode = true;
                  break;
                }
              }

              if (!existTrayCode) {
                bool hideToast = false;
                for (int j = 0; j < size; j++) {
                  String orderNo = _listOrderNo[j];
                  String trayCode = _listTrayCode[j];
                  String pickTrayId = _listPickTrayId[j];

                  if (trayCode == "?") {
                    if (listTrayStatus == 0) {
                      hideToast = true;
                      keepTrayId = listTrayId;

                      _listPreTrayId[j] = listTrayId;
                      _listPreTrayCode[j] = listTrayCode;




                      String responseTwoInString = "";

                      final responseTwo = await cubit.assignTrayToOrderPick(pickTrayId, listTrayId);
                      responseTwoInString = await responseTwo; // https://www.titanwolf.org/Network/q/81325b77-2214-47ee-8830-86a060b92a41/y

                      Map decoded = jsonDecode(responseTwoInString);
                      String retInTwo = decoded['ret'];
                      if (retInTwo == "0") {


                        String responseThreeInString = "";
                        final responseThree = await cubit.updateTrayStatus(keepTrayId);
                        responseThreeInString = await responseThree;

                        var resThree = json.decode(responseThreeInString);
                        UpdateTrayStatus _resThreeModel = UpdateTrayStatus.fromJson(resThree);

                        if (_resThreeModel.ret == "0") {
                          int sizeInList = _listNo.length;
                          for (int k = 0; k < sizeInList; k++) {
                            String trayCode = _listTrayCode[k];
                            String preTrayId = _listPreTrayId[k];
                            String? preTrayCode = _listPreTrayCode[k];

                            if (trayCode == "?") {
                              if(preTrayCode.isEmpty) {

                              }
                              else {
                                _listPickTrayId[k] = preTrayId;
                                _listTrayCode[k] = listTrayCode;
                              }
                              break;
                            }
                          }

                          await pr.hide();
                          showToast("Scanned Successful", Toast.LENGTH_SHORT,
                              ToastGravity.BOTTOM);


                          bool isShowButton = true;
                          for (int i = 0; i < _listNo.length; i++) {
                            String currentTrayCode = _listTrayCode[i];
                            if (currentTrayCode == "?") {
                              isShowButton = false;
                            }
                          }

                          if (isShowButton) {
                            setState(() {
                              isVisible = true;
                            });
                          } else {
                            setState(() {
                              isVisible = false;
                            });
                          }
                        }
                      }
                      else {
                        await pr.hide();
                        showToast("Please Try Again", Toast.LENGTH_SHORT,
                            ToastGravity.BOTTOM);
                      }

                      break;

                    } else {
                      hideToast = true;
                      await pr.hide();
                      showToast("Please Scan Valid Tray Code", Toast.LENGTH_SHORT,
                          ToastGravity.BOTTOM);
                      break;
                    }
                  }
                }

                if (!hideToast) {
                  await pr.hide();
                  showToast("All orders already linking completed", Toast.LENGTH_SHORT,
                      ToastGravity.BOTTOM);
                }
              } else {
                await pr.hide();
                showToast("Tray Code already exist", Toast.LENGTH_SHORT,
                    ToastGravity.BOTTOM);
              }
            }
          }
        }
      } catch (e) {
        print('error caught: $e');
        await pr.hide();
        showToast("Please Scan Valid Tray Code", Toast.LENGTH_SHORT,
            ToastGravity.BOTTOM);
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
