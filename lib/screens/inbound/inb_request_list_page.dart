import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:wms_app/bloc/inbound/request_detail_cubit.dart';
import 'package:wms_app/bloc/inbound/request_list_cubit.dart';
import 'package:wms_app/model/inbound/requestlist/get_inb_request_list_model.dart';
import 'package:wms_app/repo/inb_repo.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:intl/intl.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'inb_request_detail_page.dart';

class InbRequestListPage extends StatefulWidget {
  @override
  _InbRequestListPageState createState() => _InbRequestListPageState();
}

class _InbRequestListPageState extends State<InbRequestListPage> {
  late RequestListCubit cubit;
  List<String> _listInboundId = [];
  List<String> _listInboundNo = [];
  List<String> _listInboundDate = [];
  List<String> _listCompanyName = [];
  List<String> _listSkuQty = [];
  List<String> _listItemQty = [];
  String keepInboundId = "", keepInboundNo = "", keepTotalQty = "";

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      cubit.getInboundRequestList();
    });
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<RequestListCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataRequestListState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            var res = json.decode(responseInString);
            GetInbRequestList _res = GetInbRequestList.fromJson(res);
            GetInbRequestListData _resData =
                GetInbRequestListData.fromJson(_res.data.toJson());

            for (int i = 0; i < _resData.records.length; i++) {
              String no = (i + 1).toString();

              String inboundId = _resData.records[i].id.toString();
              String inboundNo = _resData.records[i].inboundNo;
              String inboundDate = _resData.records[i].inboundDate;
              String clientName = _resData.records[i].clientName;
              String itemQty = _resData.records[i].skuQty.toString();
              String skuQty = _resData.records[i].inboundQty.toString();

              String inboundDateFormat = "";
              if (inboundDate == "") {
                inboundDateFormat = "-";
              } else {
                if (! inboundDate.isEmpty) {
                  inboundDateFormat = inboundDate.substring(0, 10);
                } else {
                  inboundDateFormat = "-";
                }
              }

              _listInboundId.add(inboundId);
              _listInboundNo.add(inboundNo);
              _listInboundDate.add(inboundDateFormat);
              _listCompanyName.add(clientName);
              _listSkuQty.add(skuQty);
              _listItemQty.add(itemQty);
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,

                        // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_back.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {
                                    Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                        builder: (BuildContext context) =>
                                            GenActionPage(),
                                      ),
                                      (route) => false,
                                    );
                                  },
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: Ink(
                                  height: 60,
                                  padding: const EdgeInsets.all(5.0),
                                  decoration: ShapeDecoration(
                                    color: ColorConstants.kThemeColor,
                                    shape: CircleBorder(),
                                  ),
                                  child: IconButton(
                                    icon: Image.asset(
                                      'images/icon_topbar_scan.png',
                                      color: Colors.white ,
                                    ),
                                    onPressed: () {
                                      scanQr();
                                    },
                                  ),
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 1,
                              child: Container(
                                child: IconButton(
                                  iconSize: 20,
                                  padding: const EdgeInsets.all(15.0),
                                  icon: Image.asset(
                                    'images/icon_topbar_menu.png',
                                    color: Colors.black,
                                  ),
                                  onPressed: () {

                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      )),

                  // top
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    margin: const EdgeInsets.only(bottom: 100.0, top: 20.0, left: 20.0, right: 20.0),
                    child: Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      elevation: 10,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          // page header
                          Container(
                              width: MediaQuery.of(context).size.width,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0)),
                                color: ColorConstants.kAssignTrayTopBgColor,
                              ),
                              height: 50,
                              child: Center(
                                child: Text(
                                  'Inbound',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color:
                                          ColorConstants.kAssignTrayHeaderColor,
                                      fontSize: 17,

                                      // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w400),
                                ),
                              )),

                          // listview
                          Expanded(
                            child: Container(
                              child: ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: _listInboundId.length,
                                  separatorBuilder:
                                      (BuildContext context, int index) =>
                                          Divider(
                                              height: 1,
                                              color: ColorConstants
                                                  .kInbListLineColor),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Column(
                                      children: [
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                              flex: 2,
                                              child: GestureDetector(
                                                onTap: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context) => BlocProvider(
                                                    create: (context) => RequestDetailCubit(InboundRepository()),
                                                    child: InbRequestDetailPage(
                                                        inboundId: _listInboundId[index],
                                                        inboundNo: _listInboundNo[index],
                                                        totalQty: _listSkuQty[index]),
                                                  )
                                                  ));
                                                },
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    padding: const EdgeInsets.only(bottom: 40.0),
                                                    // margin: const EdgeInsets.only(bottom: 10.0),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(
                                                              left: 20.0, top: 30.0),
                                                          child: Text(
                                                            _listInboundNo[index],
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 12,
                                                                fontFamily: 'Montserrat',
                                                                fontWeight:
                                                                    FontWeight.w600),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(
                                                              left: 20.0, top: 20.0),
                                                          child: Text(
                                                            _listCompanyName[index],
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 14,
                                                                fontFamily: 'Montserrat',
                                                                fontWeight:
                                                                FontWeight.w400),
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: GestureDetector(
                                                onTap: () {
                                                  Navigator.push(context, MaterialPageRoute(builder: (context) => BlocProvider(
                                                    create: (context) => RequestDetailCubit(InboundRepository()),
                                                    child: InbRequestDetailPage(
                                                        inboundId: _listInboundId[index],
                                                        inboundNo: _listInboundNo[index],
                                                        totalQty: _listSkuQty[index]),
                                                  )
                                                  ));
                                                },
                                                child: Container(
                                                    width: MediaQuery.of(context).size.width,
                                                    padding: const EdgeInsets.only(bottom: 10.0),
                                                    // margin: const EdgeInsets.only(bottom: 10.0),
                                                    child: Column(
                                                      children: [
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(
                                                              right: 20.0, top: 30.0),
                                                          child: Text(
                                                            _listInboundDate[index],
                                                            textAlign: TextAlign.end,
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 12,
                                                                fontFamily:
                                                                    'Montserrat',
                                                                fontWeight:
                                                                    FontWeight.w600),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(
                                                              right: 20.0, top: 20.0),
                                                          child: Text(
                                                            _listSkuQty[index] + ' SKUs',
                                                            textAlign: TextAlign.end,
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                'Montserrat',
                                                                fontWeight:
                                                                FontWeight.w400),
                                                          ),
                                                        ),
                                                        Container(
                                                          width: MediaQuery.of(context).size.width,
                                                          margin: const EdgeInsets.only(
                                                              right: 20.0),
                                                          child: Text(
                                                            _listItemQty[index] + ' Items',
                                                            textAlign: TextAlign.end,
                                                            style: TextStyle(
                                                                color: ColorConstants
                                                                    .kInbListAdapterTextColor,
                                                                fontSize: 14,
                                                                fontFamily:
                                                                'Montserrat',
                                                                fontWeight:
                                                                FontWeight.w400),
                                                          ),
                                                        ),
                                                      ],
                                                    )),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    );
                                  }),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => BlocProvider(
                    create: (context) =>
                        RequestDetailCubit(InboundRepository()),
                    child: InbRequestDetailPage(
                        inboundId: keepInboundId, inboundNo: keepInboundNo, totalQty: keepTotalQty),
                  )));
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
