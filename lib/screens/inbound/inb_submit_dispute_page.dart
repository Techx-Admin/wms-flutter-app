import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/bloc/inbound/submit_dispute_cubit.dart';
import 'package:wms_app/model/inbound/submitdispute/body_inb_submit_dispute_model.dart';
import 'package:wms_app/model/inbound/submitdispute/get_inb_request_dispute_model.dart';
import 'package:wms_app/model/inbound/submitdispute/update_inb_dispute_bulk_model.dart';
import 'package:wms_app/model/inbound/submitdispute/update_inb_request_status_model.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'inb_request_complete_page.dart';

class InbSubmitDisputePage extends StatefulWidget {
  String inboundId;

  InbSubmitDisputePage({Key? key, required this.inboundId}) : super(key: key);

  @override
  _InbSubmitDisputePageState createState() => _InbSubmitDisputePageState();
}

class _InbSubmitDisputePageState extends State<InbSubmitDisputePage> {
  late SubmitDisputeCubit cubit;
  bool isVisibleBtnSubmit = false;
  bool isVisibleContentEmpty = false;
  List<String> _listDisputeId = [];
  List<String> _listSkuNo = [];
  List<String> _listSkuCode = [];
  List<String> _listBarcode = [];
  List<String> _listTotalQty = [];
  List<String> _listInboundQty = [];
  List<String> _listDisputeQty = [];
  List<String> _listRemark = [];
  List<String> _listStatus = [];
  late ProgressDialog progressDialog;
  String sharedBarcode = "", sharedTotalQty = "";
  int doneUpdate = 0;
  List<TextEditingController> textListRemark = [];

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sharedBarcode = prefs.getString('barcode').toString();
    sharedTotalQty = prefs.getString('totalQty').toString();

    cubit.getInboundRequestDispute();
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<SubmitDisputeCubit>();
    progressDialog = ProgressDialog(context);

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataSubmitDisputeState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            var res = json.decode(responseInString);
            GetInbRequestDispute _res = GetInbRequestDispute.fromJson(res);
            GetInbRequestDisputeData _resData = GetInbRequestDisputeData.fromJson(_res.data.toJson());

            for (int i = 0; i < _resData.inboundRequestDispute.length; i++) {
              String disputeId = _resData.inboundRequestDispute[i].id.toString();
              String skuId = _resData.inboundRequestDispute[i].inboundRequestItem.skuId.toString();
              String skuCode = _resData.inboundRequestDispute[i].inboundRequestItem.sku.skuCode;
              String barcode = _resData.inboundRequestDispute[i].inboundRequestItem.sku.barcode1;
              int totalQty = _resData.inboundRequestDispute[i].oriAmt;
              int disputeQty = _resData.inboundRequestDispute[i].disputeAmt;
              int inboundQty = totalQty - disputeQty;

              int status = _resData.inboundRequestDispute[i].status;
              String reason = _resData.inboundRequestDispute[i].reason;

              _listDisputeId.add(disputeId);
              _listSkuNo.add(skuId);
              _listSkuCode.add(skuCode);
              _listBarcode.add(barcode);
              _listTotalQty.add(totalQty.toString());
              _listInboundQty.add(inboundQty.toString());
              _listDisputeQty.add(disputeQty.toString());
              _listRemark.add(reason);
              _listStatus.add(status.toString());

              if (status == 1) {
                doneUpdate = 1;
              }
            }

            if (doneUpdate == 1) {
              isVisibleBtnSubmit = false;
            }

            if (_listDisputeId.length > 0) {

              if (doneUpdate == 0) {
                isVisibleBtnSubmit = true;

                setState(() {

                });
              }
            }
            else {
              isVisibleContentEmpty = true;

              setState(() {

              });

              updateStatus();
            }
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 120,

                        // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                        child: Column(
                          children: [
                            Visibility(
                              visible: isVisibleBtnSubmit,
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: ColorConstants.kGenPicklistButtonTextColor,
                                          fontSize: 15,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      submitData();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty.all(ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        // Navigator.pop(context, widget.requestId);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  //listview data
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 20.0),
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listDisputeId.length,
                          itemBuilder: (BuildContext context, int index) {
                            textListRemark.add(new TextEditingController());

                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  child: Card(
                                    margin: const EdgeInsets.only(top: 20.0,
                                        left: 20.0,
                                        right: 20.0 ),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    elevation: 10,
                                    child: Column(
                                      children: <Widget>[

                                        // listview header
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(10.0),
                                                  topRight: Radius.circular(10.0)),
                                              color: ColorConstants.kAssignTrayTopBgColor,
                                            ),
                                            child: Column(
                                              children: [

                                                // show sku code
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 10.0,
                                                      right: 10.0,
                                                      top: 10.0),
                                                  child: Text(
                                                    _listSkuCode[index],
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 17,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight: FontWeight
                                                            .w400),
                                                  ),
                                                ),

                                                // show inbound and total qty
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                    left: 10.0,
                                                    right: 10.0,
                                                    top: 5.0,
                                                    bottom: 15.0,),

                                                  // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                                  child: RichText(
                                                    textAlign: TextAlign.center,
                                                    text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                              text: "Quantity : ",
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontSize: 14,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: _listInboundQty[index],
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbRedColor,
                                                                  fontSize: 14,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: "/",
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontSize: 14,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: _listTotalQty[index],
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontSize: 14,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                        ]),
                                                  ),
                                                ),

                                              ],
                                            )),

                                        // missing qty
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                            left: 20.0,
                                            right: 20.0,
                                            top: 20.0,
                                            bottom: 5.0,),

                                          // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                          child: RichText(
                                            textAlign: TextAlign.end,
                                            text: TextSpan(
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text: "Missing ",
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kInbRedColor,
                                                          fontSize: 13,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight
                                                              .w600
                                                      )),
                                                  TextSpan(
                                                      text: _listDisputeQty[index],
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kInbRedColor,
                                                          fontSize: 13,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight
                                                              .w600
                                                      )),
                                                  TextSpan(
                                                      text: " Item(s)",
                                                      style: TextStyle(
                                                          color: ColorConstants
                                                              .kInbRedColor,
                                                          fontSize: 13,
                                                          fontFamily: 'Montserrat',
                                                          fontWeight: FontWeight
                                                              .w600
                                                      )),
                                                ]),
                                          ),
                                        ),

                                        // remark
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0,
                                                bottom: 20.0),
                                            child: TextField(
                                              controller: textListRemark[index],
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Epilogue',
                                                  fontWeight: FontWeight.w400),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(),
                                              ),
                                              textCapitalization: TextCapitalization.sentences,
                                              keyboardType: TextInputType.multiline,
                                              minLines: 3,
                                              maxLines: null
                                            )
                                        ),

                                      ],
                                    ),
                                  ),
                                )
                              ],
                            );
                          }),
                    ),
                  ),

                  // top with empty
                  Visibility(
                    visible: isVisibleContentEmpty,
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      margin: const EdgeInsets.only(bottom: 150.0),
                      child: Card(
                        margin: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          children: <Widget>[

                            // page header
                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                child: Column(
                                  children: [

                                    // show barcode
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 10.0, top: 15.0),
                                      child: Text(
                                        '',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 17,
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),

                                    // show qty
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(
                                        left: 10.0,
                                        right: 10.0,
                                        top: 5.0,
                                        bottom: 15.0,),

                                      // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: "Quantity : ",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbBatchQtyTextColor,
                                                  fontSize: 14,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                          TextSpan(
                                              text: "-",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbRedColor,
                                                  fontSize: 14,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                          TextSpan(
                                              text: "/-",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbBatchQtyTextColor,
                                                  fontSize: 14,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                        ]),
                                      ),
                                    ),

                                  ],
                                )),

                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 200,
                                child: Center(
                                  child: Text(
                                    'No Dispute found',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kInbListAdapterTextColor,
                                        fontSize: 12,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),

                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> submitData() async {
    bool listIsEmptyRemark = false;
    int listSize = _listDisputeId.length;

    for (int i = 0; i < _listDisputeId.length ; i++) {
      String listRemark = textListRemark[i].text;

      if (listRemark.isEmpty) {
        listIsEmptyRemark = true;
        break;
      }
    }

    if (listIsEmptyRemark) {
      showToast("Please enter Remark", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      for (int i = 0; i < _listDisputeId.length ; i++) {
        _listRemark[i] = textListRemark[i].text;
      }

      await progressDialog.show();

      List<BulkData> _listBulkData = [];

      for(int i = 0 ; i < _listDisputeId.length ; i++){
        String disputeId = _listDisputeId[i];
        String remark = _listRemark[i];

        _listBulkData.add(BulkData(disputeId, remark));
      }

      var details = new Map();
      details['bulkData'] = _listBulkData;

      final responseOne = await cubit.updateInboundDisputeInBulk(details);
      String responseOneInString = await responseOne;

      if (responseOneInString == '') {
        await progressDialog.hide();

        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        var res = json.decode(responseOneInString);
        UpdateInbDisputeBulk _res = UpdateInbDisputeBulk.fromJson(res);

        if (_res.ret == "0") {
          final responseTwo = await cubit.updateInboundRequestStatus();
          String responseTwoInString = await responseTwo;

          var resTwo = json.decode(responseTwoInString);
          UpdateInbRequestStatus _resTwo = UpdateInbRequestStatus.fromJson(resTwo);

          await progressDialog.hide();

          if (responseTwoInString == '') {
            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
          else {
            if (_resTwo.ret == "0") {
              showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => InbRequestCompletePage()));
            }
            else {
              showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
          }
        }
        else {
          await progressDialog.hide();

          showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
      }
    }
  }

  Future<void> updateStatus() async {
    await progressDialog.show();

    final response = await cubit.updateInboundRequestStatus();
    String responseInString = await response;

    var resTwo = json.decode(responseInString);
    UpdateInbRequestStatus _resTwo = UpdateInbRequestStatus.fromJson(resTwo);

    await progressDialog.hide();

    if (responseInString == '') {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {
      if (_resTwo.ret == "0") {
        showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => InbRequestCompletePage()));
      }
      else {
        showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
