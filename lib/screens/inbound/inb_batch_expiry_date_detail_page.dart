import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wms_app/bloc/inbound/batch_expiry_date_detail_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/model/inbound/batchcontainer/body_inb_batch_location_bulk_model.dart';
import 'package:wms_app/model/inbound/batchcontainer/create_inb_batch_location_bulk_model.dart';
import 'package:wms_app/model/inbound/batchcontainer/get_inb_batch_location_model.dart';
import 'package:wms_app/model/inbound/batchcontainer/get_inb_container_info_model.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:need_resume/need_resume.dart';
import 'package:shared_preferences/shared_preferences.dart';

class InbBatchExpiryDateDetailPage extends StatefulWidget {
  String keepItemId;
  String keepExpiryDate;
  String keepQuantity;
  int keepIncompleteLeft;

  InbBatchExpiryDateDetailPage({Key? key,
    required this.keepItemId,
    required this.keepExpiryDate,
    required this.keepQuantity,
    required this.keepIncompleteLeft
  }) : super(key: key);

  @override
  _InbBatchExpiryDateDetailPageState createState() => _InbBatchExpiryDateDetailPageState();
}

class _InbBatchExpiryDateDetailPageState extends ResumableState<InbBatchExpiryDateDetailPage> {
  late BatchExpiryDateDetailCubit cubit;
  bool isVisibleBtnSubmit = false;
  bool isVisibleContentEmpty = false;
  late String headeritemId, headerExpiryDate, headerQty;
  int doneUpdate = 0;
  List<String> _listItemId = [];
  List<String> _listContainerId = [];
  List<String> _listContainerCode = [];
  List<String> _listContainerStatus = [];
  List<String> _listQty = [];
  List<String> _listExpiryDate = [];
  List<String> _listTotalQty = [];
  List<TextEditingController> textListQty = [];
  bool isAddNewButton = false;
  bool isNotNewButton = false;

  @override
  void initState() {
    super.initState();

    headeritemId = widget.keepItemId;
    headerExpiryDate = widget.keepExpiryDate;
    headerQty = widget.keepQuantity;

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async {
    cubit.getBatchLocationWithBatchIdAndSkuId(headeritemId);
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<BatchExpiryDateDetailCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataBatchContainerState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            bool emptyFlag = false;

            _listItemId.clear();
            _listContainerId.clear();
            _listContainerCode.clear();
            _listContainerStatus.clear();
            _listQty.clear();
            _listExpiryDate.clear();
            _listTotalQty.clear();

            if (responseInString == "") {
              emptyFlag = true;
            }
            else {
              var res = json.decode(responseInString);
              GetInbBatchLocation _res = GetInbBatchLocation.fromJson(res);
              GetInbBatchLocationData _resData = GetInbBatchLocationData
                  .fromJson(_res.data.toJson());

              if (_res.ret == "0") {
                if (_resData.totalCount > 0) {
                  int itemSize = _resData.records.length;

                  for (int i = 0; i < itemSize; i++) {
                    String itemId = _resData.records[i].id.toString();
                    String containerId = _resData.records[i]
                        .containerRunningCodeId.toString();
                    String containerCode = _resData.records[i]
                        .containerRunningCode;
                    String batchQty = _resData.records[i].batchQty.toString();

                    _listItemId.add(itemId);
                    _listContainerId.add(containerId);
                    _listContainerCode.add(containerCode);
                    _listContainerStatus.add("0");
                    _listQty.add(batchQty);
                    _listExpiryDate.add(headerExpiryDate);
                    _listTotalQty.add(headerQty);
                  }

                  doneUpdate = 1;
                } else {
                  emptyFlag = true;
                }
              } else {
                emptyFlag = true;
              }
            }

            if (doneUpdate == 1) {
              isVisibleBtnSubmit = false;
            }
            else {
              if (emptyFlag) {
                isVisibleContentEmpty = true;
              }

              isVisibleBtnSubmit = true;
            }

            if(_listItemId.length > 0) {
              isAddNewButton = false;
              isNotNewButton = true;
            }
            else {
              isAddNewButton = true;
              isNotNewButton = false;
            }

            setState(() {

            });
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 120,

                        // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                        child: Column(
                          children: [
                            Visibility(
                              visible: isVisibleBtnSubmit,
                              maintainSize: true,
                              maintainAnimation: true,
                              maintainState: true,
                              child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      submitData();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty
                                          .all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        // Navigator.pop(context, widget.requestId);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Ink(
                                      height: 60,
                                      padding: const EdgeInsets.all(5.0),
                                      decoration: ShapeDecoration(
                                        color: ColorConstants.kThemeColor,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Image.asset(
                                          'images/icon_topbar_scan.png',
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          scanQr();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  //listview data
                  Expanded(
                    child: Container(
                      // margin: const EdgeInsets.only(top: 40.0),
                      margin: const EdgeInsets.only(bottom: 150.0),
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listItemId.length,
                          itemBuilder: (BuildContext context, int index) {
                            textListQty.add(new TextEditingController());

                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  // margin: const EdgeInsets.only(
                                  //     left: 60.0, right: 60.0, bottom: 35),
                                  child: Card(
                                    margin: const EdgeInsets.only(top: 20.0,
                                        left: 20.0,
                                        right: 20.0,
                                        bottom: 10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    elevation: 10,
                                    child: Wrap(
                                      children: <Widget>[

                                        // listview header
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(
                                                      10.0),
                                                  topRight: Radius.circular(
                                                      10.0)),
                                              color: ColorConstants
                                                  .kAssignTrayTopBgColor,
                                            ),
                                            child: Column(
                                              children: [

                                                // show barcode
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 10.0,
                                                      right: 10.0,
                                                      top: 10.0),
                                                  child: Text(
                                                    headerExpiryDate,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 17,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight: FontWeight
                                                            .w400),
                                                  ),
                                                ),

                                                // show qty
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                    left: 10.0,
                                                    right: 10.0,
                                                    top: 5.0,
                                                    bottom: 15.0,),

                                                  // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                                  child: RichText(
                                                    textAlign: TextAlign.center,
                                                    text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                              text: "Quantity : ",
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: headerQty,
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbRedColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                        ]),
                                                  ),
                                                ),

                                              ],
                                            )),

                                        // container code
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 30.0),
                                          child: Text(
                                            'Container Code',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0),
                                            child: TextField(
                                              controller: TextEditingController(
                                                  text: _listContainerCode[index]),
                                              enabled: false,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(),
                                              ),
                                              textInputAction: TextInputAction
                                                  .next,
                                              textCapitalization: TextCapitalization
                                                  .sentences,
                                            )
                                        ),

                                        // qty
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 20.0),
                                          child: Text(
                                            'Enter Inbound Quantity',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Visibility(
                                          visible: isAddNewButton,
                                          child: Container(
                                              width: MediaQuery
                                                  .of(context)
                                                  .size
                                                  .width,
                                              margin: const EdgeInsets.only(
                                                  left: 20.0,
                                                  right: 20.0,
                                                  top: 5.0,
                                                  bottom: 30.0),
                                              child: TextField(
                                                controller: textListQty[index],
                                                enabled: true,
                                                style: TextStyle(
                                                    color: ColorConstants.kPrimaryColor,
                                                    fontSize: 16,
                                                    fontFamily: 'Montserrat',
                                                    fontWeight: FontWeight.w400),
                                                decoration: InputDecoration(
                                                  border: OutlineInputBorder(),
                                                ),
                                                textInputAction: TextInputAction.done,
                                                keyboardType: TextInputType.number,
                                                inputFormatters: <TextInputFormatter>[
                                                  FilteringTextInputFormatter.digitsOnly
                                                ],
                                              )
                                          ),
                                        ),
                                        Visibility(
                                          visible: isNotNewButton,
                                          child: Container(
                                            width: MediaQuery.of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0,
                                                bottom: 30.0),
                                            child: Text(
                                              _listQty[index],
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                            ),
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                )
                              ],
                            );
                          }),
                    ),
                  ),

                  // top with empty
                  Visibility(
                    visible: isVisibleContentEmpty,
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      margin: const EdgeInsets.only(bottom: 150.0),
                      child: Card(
                        margin: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          children: <Widget>[

                            // page header
                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                child: Column(
                                  children: [

                                    // show expiry date
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 10.0, top: 10.0),
                                      child: Text(
                                        widget.keepExpiryDate,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 17,
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),

                                    // show qty
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(left: 10.0,
                                        right: 10.0,
                                        top: 5.0,
                                        bottom: 15.0,),

                                      // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: "Quantity : ",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbBatchQtyTextColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                          TextSpan(
                                              text: widget.keepQuantity,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbRedColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                        ]),
                                      ),
                                    ),

                                  ],
                                )),

                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 200,
                                child: Center(
                                  child: Text(
                                    'No Container Code yet ?',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kInbListAdapterTextColor,
                                        fontSize: 12,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),

                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {

      if(doneUpdate == 0) {
        final response = await cubit.getContainerInfoApi(barcode);
        String responseInString = await response;

        var res = json.decode(responseInString);
        GetInbContainerInfo _res = GetInbContainerInfo.fromJson(res);

        bool validContainer = false;

        if (responseInString == "") {
          showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        } else {
          if (_res.ret == "0") {

            GetInbContainerInfoData _resData = GetInbContainerInfoData.fromJson(_res.data.toJson());

            if (_resData.totalCount == 0) {
              showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
            else {
              int inboundSize = _resData.records.length;

              for (int i = 0; i < inboundSize; i++) {
                String listContainerId = _resData.records[i].id.toString();
                String listContainerCode = _resData.records[i].containerRunningCode;
                int listStatus = _resData.records[i].status;

                if (listContainerCode == barcode && listStatus == 0) {
                  _listItemId.add("");
                  _listContainerId.add(listContainerId);
                  _listContainerCode.add(listContainerCode);
                  _listContainerStatus.add("1");
                  _listQty.add("");
                  _listExpiryDate.add(headerExpiryDate);
                  _listTotalQty.add(headerQty);

                  validContainer = true;

                  break;
                }
              }

              if (validContainer) {
                isVisibleContentEmpty = false;

                setState(() {

                });
              }
              else {
                showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
            }
          } else {
            showToast("Please Scan Valid Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      }
      else{
        showToast("Currently cannot add Container Code", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
  }

  Future<void> submitData() async {

    int size = _listItemId.length;

    if(size == 0) {
      showToast("Please add Container Code before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {
      // check qty inside list
      // if qty = pending, ask user to key in
      // if qty = 0, ask user to key in more than 0
      bool listIsEqualZero = false;
      bool listIsInPending = false;
      for(int i = 0 ; i < size ; i++){
        if(textListQty[i].text.isEmpty) {
          listIsInPending = true;
          break;
        }
        else {
          if(textListQty[i].text == "0") {
            listIsEqualZero = true;
            break;
          }
        }
      }

      if(listIsInPending) {
        showToast("Please add Quantity before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
      else {
        if (listIsEqualZero) {
          showToast("Please enter Quantity more than 0", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
        }
        else {
          // check total qty inside list that must equal qty for selected expiry date
          int totalInboundQty = int.parse(headerQty);
          int currentTotalQty = 0;
          for (int i = 0; i < _listItemId.length; i++) {
            // int listQty = int.parse(_listQty[i]);
            int listQty = int.parse(textListQty[i].text);
            currentTotalQty = currentTotalQty + listQty;
          }

          if (totalInboundQty == currentTotalQty) {

            SharedPreferences prefs = await SharedPreferences.getInstance();
            String batchId = prefs.getString('batchId').toString();

            List<BulkData> _listBulkData = [];

            for(int i = 0 ; i < size ; i++){
              String listItemId = _listItemId[i];
              String listContainerId = _listContainerId[i];
              String listQty = textListQty[i].text ;

              if(listItemId.isEmpty) {
                _listBulkData.add(BulkData(listContainerId, headeritemId, listQty));
              }
            }

            var details = new Map();
            details['bulkData'] = _listBulkData;
            details['batchId'] = batchId;

            final responseOne = await cubit.createBatchLocationInBulk(details);
            String responseOneInString = await responseOne;

            if (responseOneInString == "") {
              showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            }
            else {
              var resOne = json.decode(responseOneInString);

              CreateInbBatchLocationBulk _resOne = CreateInbBatchLocationBulk.fromJson(resOne);

              if (_resOne.ret == "0") {
                int responseSize = _resOne.data.batchLocation.length;
                for (int j = 0; j < responseSize; j++) {
                  String responseItemId = _resOne.data.batchLocation[j].id.toString();
                  String responseContainerRunningId = _resOne.data.batchLocation[j].containerRunningCodeId;

                  int listSize = _listItemId.length;
                  for (int k = 0; k < listSize; k++) {
                    String listContainerId = _listContainerId[k];
                    String listContainerStatus = _listContainerStatus[k];

                    if (listContainerId == responseContainerRunningId && listContainerStatus == "1") {
                      _listItemId[k] = responseItemId;
                    }
                  }
                }

                if(widget.keepIncompleteLeft == 1) {
                  // 2021-8-9
                  // container status will be update in other api
                  final responseTwo = await cubit.updateInboundRequestItemStatus();
                  String responseTwoInString = await responseTwo;

                  showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                  Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (BuildContext context) =>
                          GenActionPage(),
                    ),
                        (route) => false,
                  );
                }
                else {
                  showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);

                  Navigator.pop(context, headeritemId);
                }
              }
              else {
                showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
              }
            }
          }
          else {
            showToast("Total Quantity not equal to Inbound Quantity", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
      }
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}
