import 'dart:io';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/bloc/inbound/batch_container_cubit.dart';
import 'package:wms_app/bloc/inbound/batch_expiry_date_header_cubit.dart';
import 'package:wms_app/bloc/inbound/batch_serial_no_cubit.dart';
import 'package:wms_app/model/inbound/batchserialno/body_inb_batch_item_bulk_model.dart';
import 'package:wms_app/model/inbound/batchserialno/create_inb_sku_batch_item_bulk_model.dart';
import 'package:wms_app/model/inbound/batchserialno/get_inb_bulk_serial_no_model.dart';
import 'package:wms_app/repo/inb_repo.dart';
import 'package:wms_app/utils/color_utils.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'inb_batch_container_page.dart';
import 'inb_batch_expiry_date_header_page.dart';

class InbBatchSerialNoPage extends StatefulWidget {
  @override
  _InbBatchSerialNoPageState createState() => _InbBatchSerialNoPageState();
}

class _InbBatchSerialNoPageState extends State<InbBatchSerialNoPage> {
  late BatchSerialNoCubit cubit;
  int doneUpdate = 0;
  bool isVisibleBtnSubmit = false;
  bool isVisibleBtnNext = false;
  bool isVisibleContentEmpty = false;
  List<String> _listNumbering = [];
  List<String> _listItemId = [];
  List<String> _listSerialNo = [];
  List<String> _listQuantity = [];
  List<String> _listBarcode = [];
  List<String> _listTotalQty = [];
  String sharedBarcode = "", sharedTotalQty = "";

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!.addPostFrameCallback((_) {
      callApiStartup();
    });
  }

  Future<void> callApiStartup() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    sharedBarcode = prefs.getString('barcode').toString();
    sharedTotalQty = prefs.getString('totalQty').toString();

    cubit.getSerialNoInBulk();
  }

  @override
  Widget build(BuildContext context) {
    cubit = context.watch<BatchSerialNoCubit>();

    return WillPopScope(
      onWillPop: () async {
        Navigator.of(context).pop(true);
        return true;
      },
      child: BlocListener(
        listener: (context, state) {
          if (state is LoadDataBatchSerialNoState && state.words != null) {
            String responseInString = "";
            setState(() {
              responseInString = state.words;
            });

            int inboundSize = 0;
            bool emptyFlag = false;

            _listNumbering.clear();
            _listItemId.clear();
            _listSerialNo.clear();
            _listQuantity.clear();
            _listBarcode.clear();
            _listTotalQty.clear();

            if (responseInString == "") {
              emptyFlag = true;
            }
            else {
              var res = json.decode(responseInString);
              GetInbBulkSerialNo _res = GetInbBulkSerialNo.fromJson(res);
              GetInbBulkSerialNoData _resData = GetInbBulkSerialNoData.fromJson(
                  _res.data.toJson());

              inboundSize = _resData.totalCount;

              for (int i = 0; i < inboundSize; i++) {
                String itemId = _resData.records[i].id.toString();
                String serialCode = _resData.records[i].serialCode;
                String qty = _resData.records[i].qty.toString();

                int numbering = i + 1;

                _listNumbering.add(numbering.toString());
                _listItemId.add(itemId);
                _listSerialNo.add(serialCode);
                _listQuantity.add("1");
                _listBarcode.add(sharedBarcode);
                _listTotalQty.add(sharedTotalQty);
              }

              if (inboundSize == 0) {
                emptyFlag = true;
              }
              else {
                doneUpdate = 1;
              }
            }

            if (doneUpdate == 1) {
              isVisibleBtnSubmit = false;
              isVisibleBtnNext = true;
            }
            else {
              if (emptyFlag) {
                isVisibleContentEmpty = true;
              }

              isVisibleBtnSubmit = true;
              isVisibleBtnNext = false;
            }

            setState(() {

            });
          }
        },
        bloc: cubit,
        child: Scaffold(
          body: SafeArea(
            top: true,
            child: Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              child: Stack(
                children: <Widget>[

                  // bottom
                  Positioned(
                      bottom: 20.0,
                      right: 0.0,
                      left: 0.0,
                      child: Container(
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 120,

                        // https://stackoverflow.com/questions/52784064/set-column-width-in-flutter
                        child: Column(
                          children: [
                            Visibility(
                              visible: isVisibleBtnSubmit,
                              child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      submitData();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty
                                          .all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Visibility(
                              visible: isVisibleBtnNext,
                              child: Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 10.0),
                                  child: new ElevatedButton(
                                    child: Text(
                                      'Next',
                                      style: TextStyle(
                                          color: ColorConstants
                                              .kGenPicklistButtonTextColor,
                                          fontSize: 15,

                                          // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onPressed: () {
                                      intentToNextPage();
                                    },
                                    style: ButtonStyle(
                                      shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(
                                                25.0),
                                          )),
                                      backgroundColor: MaterialStateProperty
                                          .all(
                                          ColorConstants.kThemeColor),
                                      padding: MaterialStateProperty.all(
                                          EdgeInsets.only(top: 10, bottom: 10)),
                                    ),
                                  )),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: IconButton(
                                      iconSize: 20,
                                      padding: const EdgeInsets.all(15.0),
                                      icon: Image.asset(
                                        'images/icon_topbar_back.png',
                                        color: Colors.black,
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop(true);
                                        // Navigator.pop(context, widget.requestId);
                                      },
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(
                                    child: Ink(
                                      height: 60,
                                      padding: const EdgeInsets.all(5.0),
                                      decoration: ShapeDecoration(
                                        color: ColorConstants.kThemeColor,
                                        shape: CircleBorder(),
                                      ),
                                      child: IconButton(
                                        icon: Image.asset(
                                          'images/icon_topbar_scan.png',
                                          color: Colors.white,
                                        ),
                                        onPressed: () {
                                          scanQr();
                                        },
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  flex: 1,
                                  child: Container(),
                                ),
                              ],
                            ),
                          ],
                        ),
                      )),

                  //listview data
                  Expanded(
                    child: Container(
                      margin: const EdgeInsets.only(top: 40.0),
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: _listNumbering.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Column(
                              children: [
                                Container(
                                  width: MediaQuery
                                      .of(context)
                                      .size
                                      .width,
                                  margin: const EdgeInsets.only(
                                      left: 60.0, right: 60.0, bottom: 35),
                                  child: Card(
                                    margin: const EdgeInsets.only(top: 20.0,
                                        left: 20.0,
                                        right: 20.0,
                                        bottom: 10.0),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    elevation: 10,
                                    child: Wrap(
                                      children: <Widget>[

                                        // listview header
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(
                                                      10.0),
                                                  topRight: Radius.circular(
                                                      10.0)),
                                              color: ColorConstants
                                                  .kAssignTrayTopBgColor,
                                            ),
                                            child: Column(
                                              children: [

                                                // show barcode
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                      left: 10.0,
                                                      right: 10.0,
                                                      top: 10.0),
                                                  child: Text(
                                                    sharedBarcode,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: ColorConstants
                                                            .kInbListAdapterTextColor,
                                                        fontSize: 17,
                                                        fontFamily: 'Montserrat',
                                                        fontWeight: FontWeight
                                                            .w400),
                                                  ),
                                                ),

                                                // show qty
                                                Container(
                                                  width: MediaQuery
                                                      .of(context)
                                                      .size
                                                      .width,
                                                  margin: const EdgeInsets.only(
                                                    left: 10.0,
                                                    right: 10.0,
                                                    top: 5.0,
                                                    bottom: 15.0,),

                                                  // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                                  child: RichText(
                                                    textAlign: TextAlign.center,
                                                    text: TextSpan(
                                                        children: <TextSpan>[
                                                          TextSpan(
                                                              text: "Quantity : ",
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbBatchQtyTextColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                          TextSpan(
                                                              text: sharedTotalQty,
                                                              style: TextStyle(
                                                                  color: ColorConstants
                                                                      .kInbRedColor,
                                                                  fontFamily: 'Montserrat',
                                                                  fontWeight: FontWeight
                                                                      .w400
                                                              )),
                                                        ]),
                                                  ),
                                                ),

                                              ],
                                            )),

                                        // serial no
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 30.0),
                                          child: Text(
                                            'Serial No',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0),
                                            child: TextField(
                                              controller: TextEditingController(
                                                  text: _listSerialNo[index]),
                                              enabled: false,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(),
                                              ),
                                              textInputAction: TextInputAction
                                                  .next,
                                              textCapitalization: TextCapitalization
                                                  .sentences,
                                            )
                                        ),

                                        // qty
                                        Container(
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width,
                                          margin: const EdgeInsets.only(
                                              left: 20.0,
                                              right: 20.0,
                                              top: 20.0),
                                          child: Text(
                                            'Enter Inbound Quantity',
                                            style: TextStyle(
                                                color: ColorConstants
                                                    .kInbDriverLabelTextColor,
                                                fontSize: 14,
                                                // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                                fontFamily: 'Montserrat',
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Container(
                                            width: MediaQuery
                                                .of(context)
                                                .size
                                                .width,
                                            margin: const EdgeInsets.only(
                                                left: 20.0,
                                                right: 20.0,
                                                top: 5.0,
                                                bottom: 30.0),
                                            child: TextField(
                                              controller: TextEditingController(
                                                  text: _listQuantity[index]),
                                              enabled: false,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kPrimaryColor,
                                                  fontSize: 16,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400),
                                              decoration: InputDecoration(
                                                border: OutlineInputBorder(),
                                              ),
                                              textInputAction: TextInputAction
                                                  .done,
                                              keyboardType: TextInputType
                                                  .number,
                                              inputFormatters: <
                                                  TextInputFormatter>[
                                                FilteringTextInputFormatter
                                                    .digitsOnly
                                              ],
                                            )
                                        ),

                                      ],
                                    ),
                                  ),
                                )
                              ],
                            );
                          }),
                    ),
                  ),

                  // top with empty
                  Visibility(
                    visible: isVisibleContentEmpty,
                    child: Container(
                      width: MediaQuery
                          .of(context)
                          .size
                          .width,
                      height: MediaQuery
                          .of(context)
                          .size
                          .height,
                      margin: const EdgeInsets.only(bottom: 150.0),
                      child: Card(
                        margin: const EdgeInsets.only(
                            top: 20.0, left: 20.0, right: 20.0, bottom: 10.0),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        elevation: 10,
                        child: Column(
                          children: <Widget>[

                            // page header
                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10.0),
                                      topRight: Radius.circular(10.0)),
                                  color: ColorConstants.kAssignTrayTopBgColor,
                                ),
                                child: Column(
                                  children: [

                                    // show barcode
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(
                                          left: 10.0, right: 10.0, top: 10.0),
                                      child: Text(
                                        sharedBarcode,
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: ColorConstants
                                                .kInbListAdapterTextColor,
                                            fontSize: 17,
                                            fontFamily: 'Montserrat',
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),

                                    // show qty
                                    Container(
                                      width: MediaQuery
                                          .of(context)
                                          .size
                                          .width,
                                      margin: const EdgeInsets.only(left: 10.0,
                                        right: 10.0,
                                        top: 5.0,
                                        bottom: 15.0,),

                                      // https://stackoverflow.com/questions/50551933/display-a-few-words-in-different-colors-in-flutter
                                      child: RichText(
                                        textAlign: TextAlign.center,
                                        text: TextSpan(children: <TextSpan>[
                                          TextSpan(
                                              text: "Quantity : ",
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbBatchQtyTextColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                          TextSpan(
                                              text: sharedTotalQty,
                                              style: TextStyle(
                                                  color: ColorConstants
                                                      .kInbRedColor,
                                                  fontFamily: 'Montserrat',
                                                  fontWeight: FontWeight.w400
                                              )),
                                        ]),
                                      ),
                                    ),

                                  ],
                                )),

                            Container(
                                width: MediaQuery
                                    .of(context)
                                    .size
                                    .width,
                                height: 200,
                                child: Center(
                                  child: Text(
                                    'No Serial No yet ?',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: ColorConstants
                                            .kInbListAdapterTextColor,
                                        fontSize: 12,

                                        // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                        fontFamily: 'Montserrat',
                                        fontWeight: FontWeight.w400),
                                  ),
                                )),

                          ],
                        ),
                      ),
                    ),
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future scanQr() async {
    await Permission.camera.request();
    String? barcode = await scanner.scan();
    if (barcode == null) {
      showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    } else {
      if (doneUpdate == 0) {
        bool result = false;

        int size = _listNumbering.length;

        for (int i = 0; i < size; i++) {
          if (barcode == _listSerialNo[i]) {
            result = true;
          }
        }

        if (result) {
          showToast("Serial No exist in the list", Toast.LENGTH_SHORT,
              ToastGravity.BOTTOM);
        }
        else {
          // Stuff that updates the UI
          int newNumbering = 0;

          if (_listNumbering.length == 0) {
            newNumbering++;
          }
          else {
            int maxSize = _listNumbering.length;
            newNumbering = maxSize + 1;
          }

          _listNumbering.add(newNumbering.toString());
          _listItemId.add("");
          _listSerialNo.add(barcode);
          _listQuantity.add("1");
          _listBarcode.add(sharedBarcode);
          _listTotalQty.add(sharedTotalQty);

          setState(() {

          });
        }
      }
      else {
        showToast("Currently cannot add Serial No", Toast.LENGTH_SHORT,
            ToastGravity.BOTTOM);
      }
    }
  }

  Future<void> submitData() async {
    int size = _listNumbering.length;

    if (size == 0) {
      showToast("Please add Serial No before submit", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
    }
    else {
      bool resultToPassApi = true;

      for (int i = 0; i < size; i++) {
        if (_listItemId[i].isEmpty) {
          resultToPassApi = false;
          break;
        }
      }

      SharedPreferences prefs = await SharedPreferences.getInstance();
      int inboundQtyInPref = int.parse(prefs.getString('inboundQty').toString());
      String batchId = prefs.getString('batchId').toString();

      if (_listNumbering.length == inboundQtyInPref) {
        if (!resultToPassApi) {
          List<BulkData> _listBulkData = [];

          for(int i = 0 ; i < size ; i++){
            String listItemId = _listItemId[i];
            if(listItemId.isEmpty) {
              String listSerialCode = _listSerialNo[i];
              String listQty = "1";
              _listBulkData.add(BulkData(listSerialCode, listQty));
            }
          }

          var details = new Map();
          details['bulkData'] = _listBulkData;
          details['batchId'] = batchId;

          final response = await cubit.createSerialNoInBulk(details);
          String responseInString = await response;

          var res = json.decode(responseInString);
          CreateInbSkuBatchItemBulk _res = CreateInbSkuBatchItemBulk.fromJson(res);

          if (_res.ret == "0") {
            CreateInbSkuBatchItemBulkList _resData = CreateInbSkuBatchItemBulkList.fromJson(_res.data.toJson());

            int responseSize = _resData.skuBatchItem.length;

            int listSize = _listItemId.length;

            for (int i = 0; i < responseSize; i++) {
              String responseItemId = _resData.skuBatchItem[i].id.toString();
              String responseSerialCode = _resData.skuBatchItem[i].serialCode;

              for (int j = 0; j < listSize; j++) {
                String listSerialCode = _listSerialNo[j];

                if(listSerialCode == responseSerialCode) {
                  _listItemId[j] = responseItemId;
                }
              }
            }

            isVisibleBtnSubmit = false;
            isVisibleBtnNext = true;

            setState(() {

            });

            showToast("Submit Successful", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
            doneUpdate = 1;

            intentToNextPage();
          }
          else {
            showToast("Please try again", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
          }
        }
        else {
          intentToNextPage();
        }
      }
      else {
        showToast("Total Serial No must equal to Inbound Quantity", Toast.LENGTH_SHORT, ToastGravity.BOTTOM);
      }
    }
  }

  Future<void> intentToNextPage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    int isSerial = int.parse(prefs.getString('skuIsSerial').toString());
    int isExpired = int.parse(prefs.getString('skuIsExpired').toString());

    if (isSerial == 1 && isExpired == 1) {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  BlocProvider(
                    create: (context) =>
                        BatchExpiryDateHeaderCubit(InboundRepository()),
                    child: InbBatchExpiryDateHeaderPage(),
                  )));
    }
    else {
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  BlocProvider(
                    create: (context) =>
                        BatchContainerCubit(InboundRepository()),
                    child: InbBatchContainerPage(),
                  )));
    }
  }

  void showToast(String msg, Toast toast, ToastGravity toastGravity) {
    setState(() {
      Fluttertoast.showToast(
          msg: msg,
          toastLength: toast,
          gravity: toastGravity,
          timeInSecForIosWeb: 1,
          backgroundColor: ColorConstants.kToastBgColor,
          textColor: ColorConstants.kToastTextColor,
          fontSize: 16.0);
    });
  }
}