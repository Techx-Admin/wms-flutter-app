import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wms_app/screens/general/gen_action_page.dart';
import 'package:wms_app/utils/color_utils.dart';

class InbRequestCompletePage extends StatefulWidget {
  @override
  _InbRequestCompletePageState createState() => _InbRequestCompletePageState();
}

class _InbRequestCompletePageState extends State<InbRequestCompletePage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: SafeArea(
          top: true,
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Stack(
              children: <Widget>[
                // center
                Align(
                    alignment: Alignment.center,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Container(
                            height: 80,
                            alignment: Alignment.center, // This is needed
                            child: Image.asset(
                              "images/icon_action_done.png",
                              fit: BoxFit.contain,
                              width: 70,
                              color: ColorConstants.kThemeColor,
                            ),
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(top: 20.0),
                              child: Text(
                                'Inbound Process Completed',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: ColorConstants.kGenPicklistTextColor,
                                    fontSize: 20,

                                    // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.w400),
                              )
                          ),
                          Container(
                              width: MediaQuery.of(context).size.width,
                              margin: const EdgeInsets.only(
                                  left: 60.0, right: 60.0, top: 20.0),
                              child: new ElevatedButton(
                                child: Text(
                                  'Done',
                                  style: TextStyle(
                                      color: ColorConstants
                                          .kGenPicklistButtonTextColor,
                                      fontSize: 15,

                                      // https://stackoverflow.com/questions/52132135/how-to-use-a-custom-font-style-in-flutter
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.w600),
                                ),
                                onPressed: () {

                                  Navigator.pushAndRemoveUntil(
                                    context,
                                    MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          GenActionPage(),
                                    ),
                                        (route) => false,
                                  );

                                },
                                style: ButtonStyle(
                                  shape: MaterialStateProperty.all<
                                          RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(25.0),
                                  )),
                                  backgroundColor: MaterialStateProperty.all(
                                      ColorConstants.kThemeColor),
                                  padding: MaterialStateProperty.all(
                                      EdgeInsets.only(top: 10, bottom: 10)),
                                ),
                              )),
                        ])),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
