import 'dart:io';
import 'dart:convert';

import 'package:wms_app/utils/string_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wms_app/service/http_service.dart';

class PutRepository {
  Future<String> getBatchWithoutLocation() async{
    var fullUrl = StringConstants.inventoryBaseUrl + StringConstants.getBatchWithoutLocation;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '1',
      'pageSize': '40',
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateBatchLocationWithStatusOne(String batchLocationId, String locationLabel) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'batchLocationId': batchLocationId,
        'locationLabel': locationLabel,
        'status': '1'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.updateBatchLocationWithStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateBatchLocationWithStatusThree(String batchLocationId, String locationLabel) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'batchLocationId': batchLocationId,
        'locationLabel': locationLabel,
        'status': '3'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.updateBatchLocationWithStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateBatchLocationWithStatusTwo(String batchLocationId, String locationLabel) async{
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String token = prefs.getString('token').toString();

      Map data = {
        'batchLocationId': batchLocationId,
        'locationLabel': locationLabel,
        'status': '2'
      };
      var body = json.encode(data);

      final response = await HttpService.postRequestWithTokenAndJson(
          4, StringConstants.updateBatchLocationWithStatus, body, token);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }
}