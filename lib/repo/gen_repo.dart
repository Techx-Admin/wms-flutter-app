
import 'dart:io';

import 'package:wms_app/service/http_service.dart';
import 'package:wms_app/utils/string_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class GeneralRepository {
  Future<String> callLogin(String userName, String password) async{
      try {
        Object body = {'email': '$userName', 'password': '$password'};
        final response = await HttpService.postRequest(1, StringConstants.userLoginUrl, body);

        if(response.statusCode == 200) {
          return response.body;
        }
        else {
          return '';
        }
      } on Exception catch (e) {
        return '';
      }
  }

  Future<String> callListWarehouse() async{
    var fullUrl = StringConstants.warehouseBaseUrl + StringConstants.getWarehouseUrl;

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token').toString();

    Map<String, String> qParams = {
      'page': '0',
      'pageSize': '40',
    };
    Map<String, String> header = {
      HttpHeaders.authorizationHeader: 'Bearer $token',
    };

    Uri uri = Uri.parse(fullUrl);
    final finalUri = uri.replace(queryParameters: qParams);

    try {
      var response = await HttpService.getRequest(finalUri, header);

      if(response.statusCode == 200) {
        return response.body;
      }
      else {
        return '';
      }
    } on Exception catch (e) {
      return '';
    }
  }
}