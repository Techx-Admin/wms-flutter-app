import 'package:http/http.dart' as http;
import 'package:wms_app/utils/string_utils.dart';

class HttpService{
  static Future<http.Response> getRequest(Uri finalUri, Map<String, String> header) async{
    http.Response response;

    try {
      response = await http.get(finalUri, headers: header);
    } on Exception catch (e) {
      throw e;
    }

    return response;
  }

  // for login api without token
  static Future<http.Response> postRequest(module, endPoint, Object? body) async{
    http.Response response;

    var baseUrl = getUrlFromModule(module);

    final url = Uri.parse("$baseUrl$endPoint");

    try {
      response = await http.post(url, body: body);
    } on Exception catch (e) {
      throw e;
    }

    return response;
  }

  static Future<http.Response> postRequestWithTokenAndJson(module, endPoint, Object? body, String token) async{
    http.Response response;

    var baseUrl = getUrlFromModule(module);

    final url = Uri.parse("$baseUrl$endPoint");

    try {
      response = await http.post(url, body: body, headers: { 'Authorization': 'Bearer $token', "Content-Type": "application/json"});
    } on Exception catch (e) {
      throw e;
    }

    return response;
  }

  static String getUrlFromModule(int module){
    if(module == 1) {
      return StringConstants.authBaseUrl;
    }
    else if(module == 2) {
      return StringConstants.InboundBaseUrl;
    }
    else if(module == 3) {
      return StringConstants.warehouseBaseUrl;
    }
    else if(module == 4) {
      return StringConstants.inventoryBaseUrl;
    }
    else {
      return StringConstants.orderBaseUrl;
    }
  }
}