import 'package:flutter/material.dart';
import 'package:wms_app/screens/general/gen_splash_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter BLoC counter demo',

      // https://www.geeksforgeeks.org/safearea-in-flutter/
      home: SafeArea(
          top: true,
          child: GenSplashPage(),
      )
    );
  }
}