
class StringConstants {
  // base url
  static String authBaseUrl = "https://wms-auth-dev.epost.tech/api/auth/";
  static String InboundBaseUrl = "https://wms-inbound-dev.epost.tech/api/inbound/";
  static String warehouseBaseUrl = "https://wms-warehouse-dev.epost.tech/api/warehouse/";
  static String inventoryBaseUrl = "https://wms-inventory-dev.epost.tech/api/inventory/";
  static String orderBaseUrl = "https://wms-order-dev.epost.tech/api/order/";

  // gen
  static String userLoginUrl = "login";
  static String getWarehouseUrl = "warehouse";
  // inb
  static String getInboundRequestList = "inbound-request";
  static String getInboundRequestDetail = "inbound-request-details";
  static String getInboundDriverList = "inbound-request-delivery";
  static String createInboundDelivery = "inbound-request-delivery/create";
  static String updateInboundDelivery = "inbound-request-delivery/update";
  static String getSkuInfo = "sku";
  static String getSkuBatchInfo = "sku-batch";
  static String getSkuLocationId = "sku-location";
  static String createSkuBatch = "sku-batch/create";
  static String createSkuLocationInSingle = "sku-location/create";
  static String createInboundDispute = "inbound-request-dispute/create";
  static String getSerialNoInBulk = "batch-item";
  static String createSerialNoInBulk = "batch-item-bulk/create";
  static String getBatchLocationWithBatchId = "batch-location";
  static String getContainerInfoApi = "container-running-code";
  static String createBatchLocationInBulk = "batch-location-bulk/create";
  static String updateInboundRequestItemStatus = "inbound-request-item/update";
  static String getInboundRequestDispute = "inbound-request-dispute";
  static String updateInboundDisputeInBulk = "inbound-request-dispute-bulk/update";
  static String updateInboundRequestStatus = "inbound-request/update";
  static String createSkuLocationInBulk = "sku-location-bulk/create";
  // put
  static String getBatchWithoutLocation = "batch-location";
  static String updateBatchLocationWithStatus = "batch-location/update";
  // pic
  static String getPickInfoUrl = "pick-info-user";
  static String checkOrderPickDetailsUrl = "check-order-pick-details";
  static String assignOrderPickUrl = "assign-order-pick";
  static String getOrderPickDetailsUrl = "order-pick/details";
  static String getTrayInfo = "tray";
  static String assignTrayToOrderPick = "assign-tray-to-order-pick";
  static String updateTrayStatus = "tray/update";
  static String getListPickItem = "order-pick-item-location";
  static String updateOrderPickItemLocationInBulk = "order-pick-item-location-bulk/update";
  static String updateOrderPick = "order-pick/update";
  static String updateOrderItemStatus = "order-item/update";
  static String updateOrderStatus = "order/update";
  // out
  static String getListOfLogisticAndTotalOrder = "order-logistic-partner";
  static String getListOfOrderCode = "order-logistic-partner-details";
  static String updateOrderStatusToSix = "order/update";

  // util
  static String util_alert_no_connection_msg = "Please check your internet connection and try again";

  // general - login
  static String login_toast_please_enter_ur_username_phone_email = "Please Enter your username / phone / email";
  static String login_toast_please_enter_ur_password = "Please Enter your password";
  static String login_toast_please_try_again = "Please try again";
  static String login_toast_login_success = "Login Successful";
  static String login_text_version = "Version";
  static String login_text_login = "Login";
  static String login_hint_enter_ur_username_phone_email = "Enter your username / phone / email";
  static String login_hint_enter_ur_password = "Enter your password";

  // general - warehouse
  static String warehouse_text_warehouse = "Warehouse";
  static String warehouse_text_loading_failed = "Loading Failed";
  static String warehouse_text_refresh = "Refresh";

  // general - action
  static String action_text_action = "Action";
  static String action_text_inbound = "Inbound"; // warehouse selection for inbound
  static String action_text_putaway = "Put-Away";
  static String action_text_picking = "Picking";
  static String action_text_outbound = "Outbound"; // to "Hand Over"

  // picking - generate picklist
  static String pick_text_outbound = "Outbound";
  static String pick_toast_not_have_available_picklist = "Not have available Pick List";

}