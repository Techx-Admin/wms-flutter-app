import 'package:crypto/crypto.dart';
import 'dart:convert';

class Method {
  static String generateMd5(String input) {
    var bytes = utf8.encode(input);
    var digest = md5.convert(bytes);
    return digest.toString();
  }
}
