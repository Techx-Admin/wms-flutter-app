import 'dart:ui';

// https://stackoverflow.com/questions/50549539/how-to-add-custom-color-to-flutter
class ColorConstants {
  static const kPrimaryColor = Color(0xFF8A8A8A);
  static const kThemeColor = Color(0xFF77c8bf);
  static const kWhiteColor = Color(0xFFF0F0F0);

  static const kToastBgColor = Color(0xFF8A8A8A);
  static const kToastTextColor = Color(0xFFF0F0F0);

  //login
  static const kButtonBgColor = Color(0xFFF0F0F0);
  static const kButtonTextColor = Color(0xFF8A8A8A);

  //warehouse
  static const kWarehouseButtonBgColor = kThemeColor;
  static const kWarehouseButtonTextColor = kWhiteColor;

  //inbound
  static const kInbListAdapterTextColor = Color(0xFF3d3d3d);
  static const kInbListLineColor = Color(0xFFCCCCCC);
  static const kInbRedColor = Color(0xFFca0c0c);
  static const kInbCircleLightRedColor = Color(0xea54551f);
  static const kInbDetailGreyColor = Color(0xFF999999);
  static const kInbDriverLabelTextColor = Color(0xFF3d3d3d);
  static const kInbBatchQtyTextColor = Color(0xFF3d3d3d);
  static const kInbRequestItemDone = Color(0xFFf5c8c7);

  //generate picklist
  static const kGenPicklistTextColor = Color(0xFF8A8A8A);
  static const kGenPicklistTextFieldColor = Color(0xFFca0c0c);
  static const kGenPicklistBorderColor = Color(0xFFe0e0e0);
  static const kGenPicklistButtonTextColor = kWhiteColor;

  //assign tray
  static const kAssignTrayTopBgColor = Color(0xFFf3f2f7);
  static const kAssignTrayLineColor = Color(0xFFCCCCCC);
  static const kAssignTrayTextColor = Color(0xFF000000);
  static const kAssignTrayHeaderColor = Color(0xFF8A8A8A);

  //Pending to Location
  static const kPendingLocationTabBgColor = Color(0xFFEBEBEB);
  static const kPendingLocationTabActiveText = kWhiteColor;
  static const kPendingLocationTabInactiveText = Color(0xFFBBBBBB);
  static const kPendingLocationTopBgColor = Color(0xFFf3f2f7);
}