import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/out_repo.dart';

class ChooseCourierCubit extends Cubit<ChooseCourierState> {
  final OutboundRepository _repository;

  ChooseCourierCubit(this._repository) : super(BeforeChooseCourierState());

  Future getListOfLogisticAndTotalOrder() async {
    emit(LoadingChooseCourierState());

    try {
      String responseFromRepo = await _repository.getListOfLogisticAndTotalOrder();

      if (responseFromRepo == '') {
        emit(FailChooseCourierState());
      } else {
        emit(LoadedChooseCourierState(responseFromRepo));
        emit(ReadyChooseCourierState());
      }
    } on Exception catch (e) {
      emit(FailChooseCourierState());
    }
  }

  Future getListOfLogisticAndTotalOrderWithoutEmit() async {
    try {
      String responseFromRepo = await _repository.getListOfLogisticAndTotalOrder();

      if (responseFromRepo == '') {
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class ChooseCourierState {}

class BeforeChooseCourierState extends ChooseCourierState { }

class LoadingChooseCourierState extends ChooseCourierState { }

class LoadedChooseCourierState extends ChooseCourierState {
  final String words;

  LoadedChooseCourierState(this.words);
}

class ReadyChooseCourierState extends ChooseCourierState { }

class FailChooseCourierState extends ChooseCourierState { }