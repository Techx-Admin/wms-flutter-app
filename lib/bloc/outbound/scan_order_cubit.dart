import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/out_repo.dart';

class ScanOrderCubit extends Cubit<ScanOrderState> {
  final OutboundRepository _repository;

  ScanOrderCubit(this._repository) : super(BeforeScanOrderState());

  Future getListOfOrderCode(String logisticName) async {
    emit(LoadingScanOrderState());

    try {
      String responseFromRepo = await _repository.getListOfOrderCode(logisticName);

      if (responseFromRepo == '') {
        emit(FailScanOrderState());
      } else {
        emit(LoadedScanOrderState(responseFromRepo));
        emit(ReadyScanOrderState());
      }
    } on Exception catch (e) {
      emit(FailScanOrderState());
    }
  }

  Future<String> updateOrderStatusToSix(String orderId) async {
    try {
      String responseFromRepo = await _repository.updateOrderStatusToSix(orderId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class ScanOrderState {}

class BeforeScanOrderState extends ScanOrderState { }

class LoadingScanOrderState extends ScanOrderState { }

class LoadedScanOrderState extends ScanOrderState {
  final String words;

  LoadedScanOrderState(this.words);
}

class ReadyScanOrderState extends ScanOrderState { }

class FailScanOrderState extends ScanOrderState { }