import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/put_repo.dart';

class PendingToLocationCubit extends Cubit<PendingToLocationState> {
  final PutRepository _repository;

  PendingToLocationCubit(this._repository) : super(BeforePendingToLocationState());

  Future getBatchWithoutLocation() async {
    emit(LoadingPendingToLocationState());

    try {
      String responseFromRepo = await _repository.getBatchWithoutLocation();

      if(responseFromRepo == '' ){
        emit(FailPendingToLocationState());
      } else {
        emit(LoadedPendingToLocationState(responseFromRepo));
        emit(ReadyPendingToLocationState());
      }
    } on Exception catch (e) {
      emit(FailPendingToLocationState());
    }
  }

  Future<String> getBatchWithoutLocationWithoutEmit() async {
    try {
      String responseFromRepo = await _repository.getBatchWithoutLocation();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateBatchLocationWithStatusThree(String batchLocationId, String locationLabel) async {
    try {
      String responseFromRepo = await _repository.updateBatchLocationWithStatusThree(batchLocationId, locationLabel);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateBatchLocationWithStatusTwo(String batchLocationId, String locationLabel) async {
    try {
      String responseFromRepo = await _repository.updateBatchLocationWithStatusTwo(batchLocationId, locationLabel);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class PendingToLocationState {}

class BeforePendingToLocationState extends PendingToLocationState { }

class LoadingPendingToLocationState extends PendingToLocationState { }

class LoadedPendingToLocationState extends PendingToLocationState {
  final String words;

  LoadedPendingToLocationState(this.words);
}

class ReadyPendingToLocationState extends PendingToLocationState { }

class FailPendingToLocationState extends PendingToLocationState { }