import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/put_repo.dart';

class CompleteMovementCubit extends Cubit<CompleteMovementState> {
  final PutRepository _repository;

  CompleteMovementCubit(this._repository) : super(ReadyCompleteMovementState());

  Future<String> updateBatchLocationWithStatusOne(String batchLocationId, String locationLabel) async {
    try {
      String responseFromRepo = await _repository.updateBatchLocationWithStatusOne(batchLocationId, locationLabel);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class CompleteMovementState {}

class ReadyCompleteMovementState extends CompleteMovementState { }

class FailCompleteMovementState extends CompleteMovementState { }