import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/inb_repo.dart';

class BatchExpiryDateHeaderCubit extends Cubit<BatchExpiryDateHeaderState>{
  final InboundRepository _repository;

  BatchExpiryDateHeaderCubit(this._repository) : super(BeforeDateHeaderState());

  Future getSkuLocationId() async {
    emit(LoadingDateHeaderState());

    try {
      String responseFromRepo = await _repository.getSkuLocationId();

      if(responseFromRepo == '' ){
        emit(FailDateHeaderState());
      } else {
        emit(LoadDataDateHeaderState(responseFromRepo));
        emit(ReadyDateHeaderState());
      }
    } on Exception catch (e) {
      emit(FailDateHeaderState());
    }
  }

  Future<String> createSkuLocationInBulk(Map details) async {
    try {
      String responseFromRepo = await _repository.createSkuLocationInBulk(details);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future getBatchLocationWithBatchIdAndSkuId(String headeritemId) async {
    try {
      String responseFromRepo = await _repository.getBatchLocationWithBatchIdAndSkuId(headeritemId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class BatchExpiryDateHeaderState {}

class BeforeDateHeaderState extends BatchExpiryDateHeaderState { }

class LoadingDateHeaderState extends BatchExpiryDateHeaderState { }

class LoadDataDateHeaderState extends BatchExpiryDateHeaderState {
  final String words;

  LoadDataDateHeaderState(this.words);
}

class ReadyDateHeaderState extends BatchExpiryDateHeaderState { }

class FailDateHeaderState extends BatchExpiryDateHeaderState { }