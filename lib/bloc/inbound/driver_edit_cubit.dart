import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/inb_repo.dart';

class DriverEditCubit extends Cubit<DriverEditState>{
  final InboundRepository _repository;

  DriverEditCubit(this._repository) : super(BeforeDriverEditState());

  Future<String> createInboundDelivery(
      String requestId, String driverName, String driverIcNo, String carPlateNo, String carParkAt) async {
    try {
      String responseFromRepo = await _repository.createInboundDelivery(
          requestId, driverName, driverIcNo, carPlateNo, carParkAt );

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateInboundDelivery(String requestId, String requestDeliveryId,
      String driverName, String driverIcNo, String carPlateNo, String carParkAt) async {
    try {
      String responseFromRepo = await _repository.updateInboundDelivery(requestId, requestDeliveryId,
          driverName, driverIcNo, carPlateNo, carParkAt);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class DriverEditState {}

class BeforeDriverEditState extends DriverEditState { }

class LoadingDriverEditState extends DriverEditState { }

class LoadDataDriverEditState extends DriverEditState {
  final String words;

  LoadDataDriverEditState(this.words);
}

class ReadyDriverEditState extends DriverEditState { }

class FailDriverEditState extends DriverEditState { }