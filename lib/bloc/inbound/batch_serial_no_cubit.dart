import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/inb_repo.dart';

class BatchSerialNoCubit extends Cubit<BatchSerialNoState>{
  final InboundRepository _repository;

  BatchSerialNoCubit(this._repository) : super(BeforeBatchSerialNoState());

  Future getSerialNoInBulk() async {
    emit(LoadingBatchSerialNoState());

    try {
      String responseFromRepo = await _repository.getSerialNoInBulk();

      if(responseFromRepo == '' ){
        emit(FailBatchSerialNoState());
      } else {
        emit(LoadDataBatchSerialNoState(responseFromRepo));
        emit(ReadyBatchSerialNoState());
      }
    } on Exception catch (e) {
      emit(FailBatchSerialNoState());
    }
  }

  Future<String> createSerialNoInBulk(Map details) async {
    try {
      String responseFromRepo = await _repository.createSerialNoInBulk(details);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class BatchSerialNoState {}

class BeforeBatchSerialNoState extends BatchSerialNoState { }

class LoadingBatchSerialNoState extends BatchSerialNoState { }

class LoadDataBatchSerialNoState extends BatchSerialNoState {
  final String words;

  LoadDataBatchSerialNoState(this.words);
}

class ReadyBatchSerialNoState extends BatchSerialNoState { }

class FailBatchSerialNoState extends BatchSerialNoState { }