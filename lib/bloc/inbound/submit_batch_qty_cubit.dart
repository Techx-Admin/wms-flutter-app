import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/inb_repo.dart';

class SubmitBatchQtyCubit extends Cubit<SubmitBatchQtyState>{
  final InboundRepository _repository;

  SubmitBatchQtyCubit(this._repository) : super(BeforeBatchQtyState());

  Future getSkuInfo(String skuId) async {
    try {
      String responseFromRepo = await _repository.getSkuInfo(skuId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future getSkuBatchInfo() async {
    try {
      String responseFromRepo = await _repository.getSkuBatchInfo();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future getSkuLocationId() async {
    try {
      String responseFromRepo = await _repository.getSkuLocationId();

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createSkuBatch(String newQty) async {
    try {
      String responseFromRepo = await _repository.createSkuBatch(newQty);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createSkuLocationInSingle(String newQty) async {
    try {
      String responseFromRepo = await _repository.createSkuLocationInSingle(newQty);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> createInboundDispute(int oriQty, int disputeQty) async {
    try {
      String responseFromRepo = await _repository.createInboundDispute(oriQty, disputeQty);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class SubmitBatchQtyState {}

class BeforeBatchQtyState extends SubmitBatchQtyState { }

class LoadingBatchQtyState extends SubmitBatchQtyState { }

class LoadDataBatchQtyState extends SubmitBatchQtyState {
  final String words;

  LoadDataBatchQtyState(this.words);
}

class ReadyBatchQtyState extends SubmitBatchQtyState { }

class FailBatchQtyState extends SubmitBatchQtyState { }