import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/gen_repo.dart';

class WarehouseCubit extends Cubit<WarehouseState>{
  final GeneralRepository _repository;

  WarehouseCubit(this._repository) : super(BeforeState());

  Future getListWarehouse() async {
    emit(LoadingState());

    try {
      String responseFromRepo = await _repository.callListWarehouse();

      if(responseFromRepo == '' ){
        emit(FailState());
      } else {
        emit(LoadDataState(responseFromRepo));
        emit(ReadyState());
      }
    } on Exception catch (e) {
      emit(FailState());
    }
  }
}

abstract class WarehouseState {}

class BeforeState extends WarehouseState { }

class LoadingState extends WarehouseState { }

class LoadDataState extends WarehouseState {
  final String words;

  LoadDataState(this.words);
}

class ReadyState extends WarehouseState { }

class FailState extends WarehouseState { }