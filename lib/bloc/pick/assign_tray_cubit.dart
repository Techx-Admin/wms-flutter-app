import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wms_app/repo/pic_repo.dart';

class AssignTrayCubit extends Cubit<AssignTrayState>{
  final PickRepository _repository;

  AssignTrayCubit(this._repository) : super(BeforeAssignTrayState());

  Future getOrderPickDetails(String pickId) async {
    emit(LoadingAssignTrayState());

    try {
      String responseFromRepo = await _repository.getOrderPickDetails(pickId);

      if(responseFromRepo == '' ){
        emit(FailAssignTrayState());
      } else {
        emit(LoadedAssignTrayState(responseFromRepo));
        emit(ReadyAssignTrayState());
      }
    } on Exception catch (e) {
      emit(FailAssignTrayState());
    }
  }

  Future<String> getTrayInfo(String trayCode) async {
    try {
      String responseFromRepo = await _repository.getTrayInfo(trayCode);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> assignTrayToOrderPick(String orderPickTrayId, String trayId) async {
    try {
      String responseFromRepo = await _repository.assignTrayToOrderPick(orderPickTrayId, trayId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }

  Future<String> updateTrayStatus(String trayId) async {
    try {
      String responseFromRepo = await _repository.updateTrayStatus(trayId);

      if(responseFromRepo == '' ){
        return '';
      } else {
        return responseFromRepo;
      }
    } on Exception catch (e) {
      return '';
    }
  }
}

abstract class AssignTrayState {}

class BeforeAssignTrayState extends AssignTrayState { }

class LoadingAssignTrayState extends AssignTrayState { }

class LoadedAssignTrayState extends AssignTrayState {
  final String words;

  LoadedAssignTrayState(this.words);
}

class ReadyAssignTrayState extends AssignTrayState { }

class FailAssignTrayState extends AssignTrayState { }